#!/bin/bash

./bin/accordion \
  -workload_path workload/workload_sample.json \
  -prometheus_endpoint http://localhost:9090 \
  -task_ranker_schedule "?/5 * * * * *"
