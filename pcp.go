package main

import (
	"bitbucket.org/sunybingcloud/accordion/metrics"
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"
)

// Headers received from PCP.
// Mapping hostname to the pcp data tag to the indices in the header.
// Example {host: myHost => {tag: 'kernel.all.cpu.user' => indices: [x, y, z ...]}}
var pcpHeaders = make(map[string]map[string][]int)

// Code taken from Electron pcp logger - https://bitbucket.org/sunybingcloud/electron-sps/src/master/pcp/pcp.go
func StartPCPLogging(
	quit *SignalChannel,
	wg *sync.WaitGroup,
	prefix string,
	collector *metrics.Collector) {

	defer wg.Done()
	const pcpCommand string = "pmdumptext -m -l -f '' -t 1.0 -d , -c config"
	filename := fmt.Sprintf("%s_%v.pcplog", prefix, time.Now().UnixNano())
	pcpLogFile, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0666)
	pcpLogFileWriter := bufio.NewWriter(pcpLogFile)

	defer func() {
		pcpLogFileWriter.Flush()
		pcpLogFile.Close()
	}()

	cmd := exec.Command("sh", "-c", pcpCommand)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	pipe, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatal(err)
	}

	scanner := bufio.NewScanner(pipe)

	stopCapping := NewSignalChannel()

	writeLine := func(line string) {
		_, err := pcpLogFileWriter.WriteString(line + "\n")
		if err != nil {
			log.Println("error writing to pcp log file: ", err)
		}
		// parsing to retrieve power consumption.
		if len(pcpHeaders) == 0 {
			// We have received the headers.
			headers := strings.Split(line, ",")
			for i, header := range headers {
				components := strings.Split(header, ":")
				host := components[0]
				// Some headers are present for each thread/core/processor.
				// In this case, we just want to get the tag corresponding to the metric.
				// A header corresponding to a single processor might be shown as <tag>[<processor Id>].
				tag := strings.Split(components[1], "[")[0]
				if _, ok := pcpHeaders[host]; !ok {
					pcpHeaders[host] = map[string][]int{
						tag: []int{i},
					}
				} else {
					pcpHeaders[host][tag] = append(pcpHeaders[host][tag], i)
				}
			}
		} else {
			// We have received the data.
			pcpDataValues := strings.Split(line, ",")
			for _, headerInfo := range pcpHeaders {
				// Calculating power usage information for host.
				var totalCpuPowerRaw float64
				var totalDramPowerRaw float64
				if raplCpuPkgIndices, ok := headerInfo["perfevent.hwcounters.rapl__RAPL_ENERGY_PKG.value"]; ok {
					for _, index := range raplCpuPkgIndices {
						if p, err := strconv.ParseFloat(pcpDataValues[index], 64); err != nil {
							// It is possible that the floating point numbers aren't
							// well formed.
							log.Println("Incorrect format in PCP data for rapl energy package!")
						} else {
							totalCpuPowerRaw += p
						}
					}
				}

				if raplDramPkgIndices, ok := headerInfo["perfevent.hwcounters.rapl__RAPL_ENERGY_DRAM.value"]; ok {
					for _, index := range raplDramPkgIndices {
						if p, err := strconv.ParseFloat(pcpDataValues[index], 64); err != nil {
							// It is possible that the floating point numbers aren't
							// well formed.
							log.Println("Incorrect format in PCP data for rapl dram package!")
						} else {
							totalDramPowerRaw += p
						}
					}
				}

				totalCpuPower := totalCpuPowerRaw * math.Pow(2, -32)
				totalDramPower := totalDramPowerRaw * math.Pow(2, -32)
				collector.ReceivePowerUsage(metrics.CpuPower, totalCpuPower)
				collector.ReceivePowerUsage(metrics.DramPower, totalDramPower)
			}
		}
	}

	wg.Add(1)
	go func(done *SignalChannel, wg *sync.WaitGroup) {
		defer wg.Done()
		// Get names of columns
		scanner.Scan()

		// Write to logfile.
		writeLine(scanner.Text())

		// Throw away first set of results.
		scanner.Scan()

		for scanner.Scan() {
			if done.IsClosed() {
				return
			}
			writeLine(scanner.Text())
		}
	}(stopCapping, wg)

	log.Println("PCP logging started")

	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	pgid, err := syscall.Getpgid(cmd.Process.Pid)

	quit.WaitTillClosed()
	stopCapping.Close()
	log.Println("PCP logging stopped")

	// http://stackoverflow.com/questions/22470193/why-wont-go-kill-a-child-process-correctly
	// kill process and all children processes
	syscall.Kill(-pgid, 15)
	return
}
