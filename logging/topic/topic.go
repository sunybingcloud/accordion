package topic

type Topic int

var (
	Other           = nameToTopic("other")            // Default topic type.
	Recommender     = nameToTopic("recommender")      // Scaling Recommender.
	ResourceUpdater = nameToTopic("resource_updater") // Resource Updater.
)

var topicNames []string

func nameToTopic(name string) Topic {
	topicNames = append(topicNames, name)
	// Returning the enumeration value of the topic.
	// This also corresponds to the index (1-indexed) of the associated name.
	return Topic(len(topicNames))
}

func (t Topic) IsValid() bool {
	return (t > 0) && (int(t) <= len(topicNames)) && (t != Other)
}

func (t Topic) String() string {
	return topicNames[t-1]
}

func FromString(s string) Topic {
	switch s {
	case "recommender":
		return Recommender
	case "resource_updater":
		return ResourceUpdater
	default:
		return Other
	}
}
