package logging

import (
	"bitbucket.org/sunybingcloud/accordion/logging/topic"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"os"
	"time"
)

const (
	// Prefix of the name of the log file to store scaling recommendation information.
	RecommenderLogFilePrefix = "recommender_logs"
	// Prefix of the name of the log file to store Resource Updater logs.
	ResourceUpdaterLogFilePrefix = "resource-updater_logs"
	// Giving everyone read and write permissions to the log files.
	logFilePermissions = 0666
)

// instantiating a Logger to be used. This instance is configured and maintained locally.
var log = logrus.New()

var recommenderLogFile *os.File
var resourceUpdaterLogFile *os.File

// createRecommenderLogFile creates the log file to which recommender logs are persisted.
func createRecommenderLogFile(now time.Time) error {
	var err error
	filename := fmt.Sprintf("%s_%v.log", RecommenderLogFilePrefix, now.UnixNano())
	recommenderLogFile, err = os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, logFilePermissions)
	if err != nil {
		err = errors.Wrap(err, "failed to create recommender log file")
	}
	return err
}

// createResourceUpdaterLogFile creates the log file to which resource updater logs are persisted.
func createResourceUpdaterLogFile(now time.Time) error {
	var err error
	filename := fmt.Sprintf("%s_%v.log", ResourceUpdaterLogFilePrefix, now.UnixNano())
	resourceUpdaterLogFile, err = os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, logFilePermissions)
	if err != nil {
		err = errors.Wrap(err, "failed to create resource updater log file")
	}
	return err
}

// Configure the logger.
func Configure() error {
	// Disabling logging to stdout.
	log.SetOutput(ioutil.Discard)
	// Setting highest log level.
	log.SetLevel(logrus.InfoLevel)

	// Creating the log files.
	now := time.Now()
	var err error

	if err = createRecommenderLogFile(now); err != nil {
		return err
	}

	if err = createResourceUpdaterLogFile(now); err != nil {
		return err
	}

	log.AddHook(newWriterHook(NewCSVFormatter(), recommenderLogFile, topic.Recommender))
	log.AddHook(newWriterHook(NewCSVFormatter(), resourceUpdaterLogFile, topic.ResourceUpdater))
	return nil
}

func Done() error {
	var err error
	if recommenderLogFile != nil {
		err = recommenderLogFile.Close()
		if err != nil {
			err = errors.Wrap(err, "failed to close recommender log file")
		}
	}

	if resourceUpdaterLogFile != nil {
		err = resourceUpdaterLogFile.Close()
		if err != nil {
			err = errors.Wrap(err, "failed to close resource updater log file")
		}
	}

	return err
}

// Aliasing logrus functions.
var WithField = log.WithField
var WithFields = log.WithFields
var Info = log.Info
var Infof = log.Infof
var Error = log.Error
var Errorf = log.Errorf
var Warn = log.Warn
var Warnf = log.Warnf
