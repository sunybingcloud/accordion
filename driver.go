package main

import (
	"bitbucket.org/sunybingcloud/accordion/def"
	aLog "bitbucket.org/sunybingcloud/accordion/logging"
	"bitbucket.org/sunybingcloud/accordion/metrics"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/pkg/errors"
	taskranker "github.com/pradykaushik/task-ranker"
	"github.com/pradykaushik/task-ranker/datafetcher"
	"github.com/pradykaushik/task-ranker/datafetcher/prometheus"
	"github.com/pradykaushik/task-ranker/query"
	"github.com/pradykaushik/task-ranker/strategies"
	"github.com/robfig/cron/v3"
	"log"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"time"
)

var workloadPath string
var promEndPoint string
var hostname string

var scalerSchedule string
var metricSchedule string
var scaleLimitCpuMax int64
var scaleLimitCpuMin int64
var metricHistoryCap int
var recommenderName string
var updaterName string
var listRecommenders bool
var listResourceUpdaters bool
var pcpConfig string
var pcpLogFilePrefix string
var profilingWindow int64
var periodicProfiling bool
var profilingIntervalCycles int64
var scale bool
var backlogPressure float64

func init() {
	flag.StringVar(&workloadPath, "workload_path", "", "path of the json file containing the workload")
	flag.StringVar(&promEndPoint, "prometheus_endpoint", "", "prometheus endpoint from where to fetch data")
	flag.StringVar(&scalerSchedule, "scalerschedule", "", "schedule (cron format) on which the scaler is to run")
	flag.StringVar(&metricSchedule, "metricschedule", "", "schedule (cron format) on which metrics are received")
	flag.Int64Var(&scaleLimitCpuMax, "maxscalelimit", int64(runtime.NumCPU()), "max threshold for vertical scaling of containers")
	flag.Int64Var(&scaleLimitCpuMin, "minscalelimit", 1, "minimum threshold for vertical scaling of containers")
	flag.IntVar(&metricHistoryCap, "metrichistorycap", 0, "size of history per metric")
	flag.StringVar(&recommenderName, "recommender", "naive", "name of the scaling recommender to use")
	flag.StringVar(&updaterName, "updater", "naive", "name of the resource updater to use")
	flag.BoolVar(&listRecommenders, "listrecommenders", false, "list available recommenders")
	flag.BoolVar(&listResourceUpdaters, "listresourceupdaters", false, "list available resource updaters")
	flag.StringVar(&pcpConfig, "pcpconfig", "config", "Name of the PCP config file")
	flag.StringVar(&pcpLogFilePrefix, "pcplogfileprefix", "accordion", "prefix of the pcp log file")
	flag.Int64Var(&profilingWindow, "profilewindow", 1, "profiling window size as number of scaling cycles")
	flag.BoolVar(&periodicProfiling, "periodicprofiling", false, "toggle periodic profiling on/off. (default off)")
	flag.Int64Var(&profilingIntervalCycles, "profilingintervalcycles", 1, "interval between two consecutive profiling "+
		"events in terms of number of scaling cycles. Periodic profiling has to be enabled (default 1).")
	//	"Periodic profiling has to be disabled.")
	flag.BoolVar(&scale, "scale", false, "toggle scaling on/off. (default off)")
	flag.Float64Var(&backlogPressure, "backlogpressure", 0.5, "portion of the remaining time to deadline that can be used "+
		"to consume any remaining backlog. (range = (0, 1]) (default 0.5)")
}

func Usage() {
	fmt.Println("Run 'make help' to know the required arguments and other options")
}

// Constant cpu period for all containers.
const CpuPeriod int64 = 100000

func MBToBytes(megabytes int) int64 {
	return int64(megabytes) * 1024 * 1024
}

func CreateAndStartContainer(
	task def.Task,
	ctx context.Context,
	cli *client.Client,
	taskID string,
	vscaler *VScaler) (container.ContainerCreateCreatedBody, error) {

	_, err := cli.ImagePull(ctx, task.Image, types.ImagePullOptions{})
	if err != nil {
		return container.ContainerCreateCreatedBody{}, err
	}
	//io.Copy(os.Stdout, reader)
	var labels map[string]string = make(map[string]string)
	labels["task_id"] = taskID
	labels["task_hostname"] = hostname
	//Tty:    false,
	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image:  task.Image,
		Cmd:    task.CMD,
		Labels: labels,
	}, &container.HostConfig{
		Resources: container.Resources{
			CPUPeriod: CpuPeriod,
			CPUQuota:  int64(task.CpuQuota) * CpuPeriod,
			Memory:    MBToBytes(task.RAM),
		},
	}, nil, nil, "")
	if err != nil {
		return container.ContainerCreateCreatedBody{}, err
	}
	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return container.ContainerCreateCreatedBody{}, err
	} else {
		if vscaler != nil {
			vscaler.NewTask(task, taskID, resp.ID, int64(task.CpuQuota)*CpuPeriod)
		}
	}
	return resp, nil
}
func ContainerCompletionWait(ctx context.Context, cli *client.Client, resp container.ContainerCreateCreatedBody) error {
	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			return err
		}
	case success := <-statusCh:
		log.Println(success)
	}
	_, err := cli.ContainerLogs(ctx, resp.ID, types.ContainerLogsOptions{ShowStdout: true})
	if err != nil {
		return err
	}
	return nil
	//stdcopy.StdCopy(os.Stdout, os.Stderr, out)
}

func InitTaskRanker(receiver *metrics.Collector) (*taskranker.TaskRanker, error) {
	var prometheusDataFetcher datafetcher.Interface
	var err error
	var tRanker *taskranker.TaskRanker

	prometheusDataFetcher, err = prometheus.NewDataFetcher(
		prometheus.WithPrometheusEndpoint(promEndPoint))
	if err != nil {
		log.Println(err)
	}

	//  Verify prometheusScrapeInterval=1s in Prometheus config else set it to the current value.
	tRanker, err = taskranker.New(
		taskranker.WithDataFetcher(prometheusDataFetcher),
		taskranker.WithSchedule(metricSchedule),
		taskranker.WithPrometheusScrapeInterval(1*time.Second),
		taskranker.WithStrategyOptions("dT-profile",
			strategies.WithLabelMatchers([]*query.LabelMatcher{
				{Type: query.TaskID, Label: "container_label_task_id", Operator: query.EqualRegex, Value: "accordion-.*"},
				{Type: query.TaskHostname, Label: "container_label_task_hostname", Operator: query.Equal, Value: hostname}}),
			strategies.WithTaskRanksReceiver(receiver)))
	return tRanker, err
}

func CleanupContainers(ctx context.Context, cli *client.Client, containers []container.ContainerCreateCreatedBody) {
	timeout := 5 * time.Second
	for _, container := range containers {
		err := cli.ContainerStop(ctx, container.ID, &timeout)
		if err != nil {
			log.Println("failed to stop the container: ", err)
		} else {
			err = cli.ContainerRemove(context.TODO(), container.ID, types.ContainerRemoveOptions{Force: true})
			if err != nil {
				log.Println("failed to kill the container: ", err)
			}
		}
	}
}

func PrintErrorAndExit(err error) {
	log.Println(err)
	log.Println("Gracefully shutting down after encountering error")
	os.Exit(1)
}

func InitVScaler(
	metricSchedule string,
	metricHistoryCap int,
	recommenderName, updaterName string,
	scalerSchedule string,
	scaleLimitCpuMax, scaleLimitCpuMin int64,
	hostMetricStore, taskMetricStore metrics.Store,
	profilingWindow int64, periodicProfiling bool, profilingIntervalCycles int64,
	backlogPressure float64,
	cli *client.Client, ctx context.Context) (*VScaler, error) {

	log.Printf("rec[%s], res_upd[%s], scalerSched[%s], minLimit[%d], maxLimit[%d]"+
		", pw[%d], bklPressure[%f], periodicProfiling[%v], profilingIntervalCycles[%d]",
		recommenderName, updaterName, scalerSchedule, scaleLimitCpuMin,
		scaleLimitCpuMax, profilingWindow, backlogPressure, periodicProfiling, profilingIntervalCycles)

	var metricSched cron.Schedule
	var err error
	if metricSchedule == "" {
		log.Fatal("invalid cron schedule provided for metrics")
	} else {
		metricSched, err = cron.NewParser(
			cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(metricSchedule)
		if err != nil {
			log.Fatal()
		}
	}

	// Validating capacity of metric history.
	if metricHistoryCap <= 0 {
		log.Fatal("metric history capacity cannot be less than 0")
	}

	// Set of options required to initialize
	// 1. vertical scaler.
	// 2. scaling recommenders.
	var vscalerOptions []VScalerOption

	vscalerRecommender, err := GetVScalerRecommender(recommenderName, metricSched, hostMetricStore, taskMetricStore, backlogPressure)
	if err != nil {
		log.Fatal(errors.Wrap(err, "failed to instantiate scaling recommender").Error())
	}

	vscalerResourceUpdater, err := GetVScalerResourceUpdater(updaterName, hostMetricStore, taskMetricStore, cli, ctx)
	if err != nil {
		log.Fatal(errors.Wrap(err, "failed to instantiate resource updater").Error())
	}

	vscalerOptions = append(vscalerOptions,
		WithSchedule(scalerSchedule),
		WithScalingLimits(scaleLimitCpuMax, scaleLimitCpuMin),
		WithRecommender(vscalerRecommender),
		WithResourceUpdater(vscalerResourceUpdater),
		WithProfilingWindow(profilingWindow),
		WithPeriodicProfiling(periodicProfiling, profilingIntervalCycles))

	return NewVScaler(vscalerOptions...)
}

func listAvailableRecommenders() {
	fmt.Println("\nRecommenders\n=============")
	for r := range recommenderFactory {
		fmt.Println(r)
	}
}

func listAvailableResourceUpdaters() {
	fmt.Println("\nResource Updaters\n====================")
	for u := range resourceUpdaterFactory {
		fmt.Println(u)
	}
}

func main() {
	log.Println("Accordion Welcomes You :)")
	flag.Parse()
	// listing available recommenders and/or resource updaters if need be.
	var listed = false
	if listRecommenders {
		listAvailableRecommenders()
		listed = true
	}
	if listResourceUpdaters {
		listAvailableResourceUpdaters()
		listed = true
	}
	if listed {
		// quitting. User just wanted to list available recommenders and/or resource updaters.
		os.Exit(0)
	}
	//Simple validation, include specific validation for path and prometheus endpoint later
	if (workloadPath == "") || (promEndPoint == "") || (metricSchedule == "") {
		Usage()
		os.Exit(1)
	}

	log.Println("validated workload path, prometheus endpoint and metric schedule")

	var err error
	hostname, err = os.Hostname()
	if err != nil {
		PrintErrorAndExit(err)
	}
	log.Println("hostname =", hostname)
	file, err := os.Open(workloadPath)
	if err != nil {
		PrintErrorAndExit(err)
	}
	decoder := json.NewDecoder(file)
	var tasks []def.Task
	if err := decoder.Decode(&tasks); err != nil {
		PrintErrorAndExit(err)
	}
	// Setting submitted time.
	now := time.Now()
	for i := 0; i < len(tasks); i++ {
		tasks[i].SubmittedTime = now
	}
	log.Println("tasks decoded and submission time recorded")

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		log.Println("Unable to create docker client")
		PrintErrorAndExit(err)
	}

	// Instantiating metric stores.
	hostMetricStore := metrics.NewHostMetricStore(metricHistoryCap)
	taskMetricStore := metrics.NewTaskMetricStore(metricHistoryCap)

	// configuring the logger.
	if err := aLog.Configure(); err != nil {
		log.Fatal(errors.Wrap(err, "failed to configure the logger"))
	}

	var listeners = []metrics.StatusListener{taskMetricStore.(*metrics.TaskMetricStore)}
	log.Println("created metric stores")
	var vscaler *VScaler
	if scale {
		log.Println("scaling turned on")
		// Instantiating vertical scaler.
		vscaler, err = InitVScaler(
			metricSchedule,
			metricHistoryCap,
			recommenderName, updaterName,
			scalerSchedule,
			scaleLimitCpuMax, scaleLimitCpuMin,
			hostMetricStore, taskMetricStore,
			profilingWindow, periodicProfiling, profilingIntervalCycles,
			backlogPressure,
			cli, ctx)
		if err != nil {
			log.Fatal(errors.Wrap(err, "failed to instantiate scaler"))
		}
		log.Println("created vertical scaler")
		listeners = append(listeners, vscaler)
	} else {
		log.Println("scaling turned off")
	}

	collector := metrics.NewCollector(hostMetricStore, taskMetricStore)
	collector.RegisterStatusListeners(listeners...)
	tRanker, err := InitTaskRanker(collector)
	if err != nil {
		log.Println("Unable to create task ranker")
		PrintErrorAndExit(err)
	}

	if scale && (vscaler != nil) {
		vscaler.Start()
	}

	tRanker.Start()
	pcpCh := NewSignalChannel()
	var wg sync.WaitGroup
	wg.Add(1)
	// Starting pcp logging.
	go StartPCPLogging(pcpCh, &wg, pcpLogFilePrefix, collector)
	var containers []container.ContainerCreateCreatedBody
	for t := range tasks {
		task := tasks[t]
		for i := 0; i < task.Instances; i++ {
			taskContainerBody, err := CreateAndStartContainer(task, ctx, cli,
				def.GenerateTaskID(task.Name, i), vscaler)
			if err != nil {
				log.Println(errors.Wrap(err, "failed to start "+task.Name+". Shutting down...").Error())
				log.Println("Stopping Accordion...")
				shutdown(ctx, cli, containers, pcpCh, tRanker, vscaler, &wg)
				log.Println("Bye!!")
				os.Exit(0)
			}
			containers = append(containers, taskContainerBody)
		}
	}

	// Handle sigint/sigkill to shutdown gracefully.
	sigHandlerCh := NewSignalChannel() // close to shutdown sigint handler.
	go func(wg *sync.WaitGroup, sigHandlerCh *SignalChannel, pcpCh *SignalChannel) {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt, os.Kill)
		f := func() int {
			signal.Stop(c)
			return 0
		}
		defer func() {
			f()
		}()
		// Polling for signal.
		for {
			if sigHandlerCh.IsClosed() {
				// If here, then we no longer have to handle SIGINT/SIGKILL
				return // main routine will shutdown accordion.
			}
			select {
			case <-c:
				log.Println("Stopping Accordion...")
				sigHandlerCh.Close()
				shutdown(ctx, cli, containers, pcpCh, tRanker, vscaler, wg)
				log.Println("Bye!!")
				os.Exit(f()) // terminating the program.
			default:
				// do nothing.
			}
		}
	}(&wg, sigHandlerCh, pcpCh)

	for _, c := range containers {
		ContainerCompletionWait(ctx, cli, c)
	}
	log.Println("Stopping Accordion...")
	sigHandlerCh.Close()
	shutdown(ctx, cli, containers, pcpCh, tRanker, vscaler, &wg)
	log.Println("Bye!!")
}

func shutdown(
	ctx context.Context,
	cli *client.Client,
	containers []container.ContainerCreateCreatedBody,
	pcpCh *SignalChannel,
	tRanker *taskranker.TaskRanker,
	vscaler *VScaler,
	wg *sync.WaitGroup) {

	// Stopping pcp logging.
	pcpCh.Close()
	// Cleaning up containers.
	CleanupContainers(ctx, cli, containers)
	// Stopping task ranker.
	tRanker.Stop()
	// Stopping vertical scaling.
	if vscaler != nil {
		vscaler.Stop()
	}
	if err := aLog.Done(); err != nil {
		log.Fatal(errors.Wrap(err, "failed to close logger"))
	}
	wg.Wait()
}
