EXECUTABLE=accordion
executable=$(EXECUTABLE)
BIN_DIR=bin

all: build

build:
	go build -v -o $(BIN_DIR)/$(executable)

build-linux:
	env GOARCH=amd64 GOOS=linux go build -v -o $(BIN_DIR)/$(executable)

run: build
	./run.sh

test:
	go test ./...

test-verbose:
	go test -v ./...

test-module:
	go test $(module)

test-module-verbose:
	go test -v $(module)

format:
	go fmt ./...

clean:
	go clean
	rm -r $(BIN_DIR)

help:
	./$(BIN_DIR)/$(executable) -h
