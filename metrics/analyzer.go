package metrics

// Analyzer to analyze the data in the metric store.
type Analyzer interface {
	VisitHostMetricStore(*HostMetricStore)
	VisitTaskMetricStore(*TaskMetricStore)
}

// AverageAnalyzer computes the average of the historic values stored for each metric type.
type AverageAnalyzer struct {
	resultHost map[Type]float64
	resultTask map[string]map[Type]float64
}

func NewAverageAnalyzer() Analyzer {
	return &AverageAnalyzer{
		resultHost: nil,
		resultTask: nil,
	}
}

func (a *AverageAnalyzer) VisitHostMetricStore(s *HostMetricStore) {
	a.resultHost = make(map[Type]float64)
	for t, data := range s.Data {
		var sum float64 = 0
		var n = 0
		data.Do(func(p interface{}) {
			if p != nil {
				sum += p.(float64)
				n++
			}
		})
		a.resultHost[t] = sum / float64(n)
	}
}

func (a *AverageAnalyzer) VisitTaskMetricStore(s *TaskMetricStore) {
	a.resultTask = make(map[string]map[Type]float64)
	for taskId, taskMetrics := range s.Data {
		a.resultTask[taskId] = make(map[Type]float64)
		for t, data := range taskMetrics {
			var sum float64 = 0
			var n = 0
			data.Do(func(p interface{}) {
				if p != nil {
					sum += p.(float64)
					n++
				}
			})
			a.resultTask[taskId][t] = sum / float64(n)
		}
	}
}

// GetResult returns the result of performing the analysis.
func (a *AverageAnalyzer) GetResult() (map[Type]float64, map[string]map[Type]float64) {
	return a.resultHost, a.resultTask
}

type TaskMetricWindowAverageAnalyzer struct {
	resultTask float64
	window     int
	metricType Type
	taskId     string
}

func NewTaskMetricWindowAverageAnalyzer(taskId string, n int, t Type) Analyzer {
	return &TaskMetricWindowAverageAnalyzer{
		window:     n,
		metricType: t,
		taskId:     taskId,
	}
}

func (a *TaskMetricWindowAverageAnalyzer) VisitHostMetricStore(s *HostMetricStore) {}

func (a *TaskMetricWindowAverageAnalyzer) VisitTaskMetricStore(s *TaskMetricStore) {
	if _, ok := s.Data[a.taskId]; !ok {
		return
	}
	if _, ok := s.Data[a.taskId][a.metricType]; !ok {
		return
	}
	var sum float64 = 0
	ptr := s.Data[a.taskId][a.metricType]
	n := ptr.Len()
	if a.window < n {
		n = a.window
	}
	var i int
	for i = 0; (i < n) && (ptr.Value != nil); i++ {
		sum += ptr.Value.(float64)
		ptr = ptr.Next()
	}
	if i < n {
		n = i
	}
	if n == 0 {
		a.resultTask = 0 // avoid divide by 0.
	}
	a.resultTask = sum / float64(n)
}

func (a *TaskMetricWindowAverageAnalyzer) GetResult() float64 {
	return a.resultTask
}
