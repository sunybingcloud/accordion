package metrics

import (
	"github.com/pradykaushik/task-ranker/entities"
	"log"
	"sync"
)

type Collector struct {
	hostMetricStore Store
	taskMetricStore Store
	// Maintaining the set of currently running tasks.
	runningTasks map[string]struct{}
	mu           sync.Mutex
	listeners    []StatusListener
}

// StatusListener defines an API for a listener of a status update.
type StatusListener interface {
	TaskTerminated(taskId string)
}

func NewCollector(hostMetricStore, taskMetricStore Store) *Collector {
	return &Collector{
		hostMetricStore: hostMetricStore,
		taskMetricStore: taskMetricStore,
		runningTasks:    make(map[string]struct{}),
		listeners:       make([]StatusListener, 0),
	}
}

func (c *Collector) RegisterStatusListeners(listeners ...StatusListener) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.listeners = append(c.listeners, listeners...)
}

// Receive task metrics from task ranker and store them in the metric store.
// Note: following hack in commit 8caea6091761594a5b1dcd8cbf2b045a41a6ff39.
// taskMetrics => {taskId: {ID: <metric>, Weight: <value>}}
func (c *Collector) Receive(taskMetrics entities.RankedTasks) {
	c.mu.Lock()
	defer c.mu.Unlock()
	for taskId, data := range taskMetrics {
		if _, ok := c.runningTasks[string(taskId)]; !ok {
			c.runningTasks[string(taskId)] = struct{}{}
		}
		for _, d := range data {
			t := TypeFromString(d.ID)
			if t.isValid() {
				err := c.taskMetricStore.(*TaskMetricStore).Take(string(taskId), t, d.Weight)
				if err != nil {
					log.Println("failed to store task metrics: ", err)
				}
			}
		}
	}

	// Notifying the stores about terminated tasks.
	var toRemove []string
	for taskId := range c.runningTasks {
		// again, this line looks a little weird due to the hack specifies above.
		if _, ok := taskMetrics[entities.Hostname(taskId)]; !ok {
			for _, l := range c.listeners {
				l.TaskTerminated(taskId)
			}
			toRemove = append(toRemove, taskId)
		}
	}

	// Removing terminated tasks from the set of running tasks.
	for _, taskId := range toRemove {
		// listeners already notified.
		delete(c.runningTasks, taskId)
	}
}

// ReceivePowerUsage receives power usage data and stores it in the metric store.
func (c *Collector) ReceivePowerUsage(t Type, powerUsage float64) {
	c.mu.Lock()
	defer c.mu.Unlock()
	err := c.hostMetricStore.(*HostMetricStore).Take(t, powerUsage)
	if err != nil {
		log.Println("failed to store power usage: ", err)
	}
}
