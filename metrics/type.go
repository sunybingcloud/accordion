package metrics

const (
	cpuPowerString                         = "cpuPower"
	dramPowerString                        = "dramPower"
	cpuSharesString                        = "container_spec_cpu_shares"
	cpuQuotaString                         = "container_spec_cpu_quota"
	cpuPeriodString                        = "container_spec_cpu_period"
	cpuUtilizationString                   = "cpu_util_percentage"
	instructionRetirementRateString        = "instruction_retirement_rate"
	cyclesPerSecondString                  = "cycles_per_second"
	cyclesPerInstructionString             = "cycles_per_instruction"
	cpuCfsThrottledPeriodsTotalString      = "container_cpu_cfs_throttled_periods_total"
	cpuCfsThrottledSecondsTotalString      = "container_cpu_cfs_throttled_seconds_total"
	cpuSchedStatRunQueueSecondsTotalString = "container_cpu_schedstat_runqueue_seconds_total"
	cpuSchedStatRunPeriodsTotalString      = "container_cpu_schedstat_run_periods_total"
	cpuSchedStatRunSecondsTotalString      = "container_cpu_schedstat_run_seconds_total"
	fsUsageBytesString                     = "container_fs_usage_bytes"
	processesString                        = "container_processes"
)

// Type of metric.
type Type int

var (
	CpuPower                         = typeToIota()
	DramPower                        = typeToIota()
	CpuShares                        = typeToIota()
	CpuQuota                         = typeToIota()
	CpuPeriod                        = typeToIota()
	CpuUtilization                   = typeToIota()
	InstructionRetirementRate        = typeToIota()
	CyclesPerSecond                  = typeToIota()
	CyclesPerInstruction             = typeToIota()
	CpuCfsThrottledPeriodsTotal      = typeToIota()
	CpuCfsThrottledSecondsTotal      = typeToIota()
	CpuSchedStatRunQueueSecondsTotal = typeToIota()
	CpuSchedStatRunPeriodsTotal      = typeToIota()
	CpuSchedStatRunSecondsTotal      = typeToIota()
	FsUsageBytes                     = typeToIota()
	Processes                        = typeToIota()
)

var types []Type

func typeToIota() Type {
	n := len(types)
	types = append(types, Type(n))
	return Type(n)
}

func (t Type) isValid() bool {
	return (t >= 0) && (int(t) < len(types))
}

func TypeFromString(s string) Type {
	switch s {
	case cpuPowerString:
		return CpuPower
	case dramPowerString:
		return DramPower
	case cpuSharesString:
		return CpuShares
	case cpuQuotaString:
		return CpuQuota
	case cpuPeriodString:
		return CpuPeriod
	case cpuUtilizationString:
		return CpuUtilization
	case instructionRetirementRateString:
		return InstructionRetirementRate
	case cyclesPerSecondString:
		return CyclesPerSecond
	case cyclesPerInstructionString:
		return CyclesPerInstruction
	case cpuCfsThrottledPeriodsTotalString:
		return CpuCfsThrottledPeriodsTotal
	case cpuCfsThrottledSecondsTotalString:
		return CpuCfsThrottledSecondsTotal
	case cpuSchedStatRunQueueSecondsTotalString:
		return CpuSchedStatRunQueueSecondsTotal
	case cpuSchedStatRunPeriodsTotalString:
		return CpuSchedStatRunPeriodsTotal
	case cpuSchedStatRunSecondsTotalString:
		return CpuSchedStatRunSecondsTotal
	case fsUsageBytesString:
		return FsUsageBytes
	case processesString:
		return Processes
	default:
		return Type(-1)
	}
}
