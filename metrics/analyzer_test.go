package metrics

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewAverageAnalyzer(t *testing.T) {
	a := NewAverageAnalyzer()
	assert.Nil(t, a.(*AverageAnalyzer).resultHost)
	assert.Nil(t, a.(*AverageAnalyzer).resultTask)
}

func TestAverageAnalyzer_VisitHostMetricStore(t *testing.T) {
	hms := mockHostMetricStore()
	// basic testing.
	assert.NotNil(t, hms)
	assert.NoError(t, hostMetricStoreTakeErr)
	avgAnalyzer := NewAverageAnalyzer()
	assert.Nil(t, avgAnalyzer.(*AverageAnalyzer).resultHost)
	hms.Accept(avgAnalyzer)
	result, _ := avgAnalyzer.(*AverageAnalyzer).GetResult()
	assert.NotNil(t, result)
	assert.Len(t, result, 1)
	avgPower, ok := result[CpuPower]
	assert.True(t, ok)
	assert.Equal(t, float64(102), avgPower)
}

func TestAverageAnalyzer_VisitTaskMetricStore(t *testing.T) {
	tms := mockTaskMetricStore()
	// basic testing.
	assert.NotNil(t, tms)
	assert.NoError(t, taskMetricStoreTakeErr)
	avgAnalyzer := NewAverageAnalyzer()
	assert.Nil(t, avgAnalyzer.(*AverageAnalyzer).resultTask)
	tms.Accept(avgAnalyzer)
	_, result := avgAnalyzer.(*AverageAnalyzer).GetResult()
	assert.NotNil(t, result)
	assert.Len(t, result, 5)
	for i := 1; i <= 5; i++ {
		taskId := fmt.Sprintf("task-%d", i)
		_, ok := result[taskId]
		assert.True(t, ok)
		avgCpuQuota, ok := result[taskId][CpuQuota]
		assert.True(t, ok)
		assert.Equal(t, float64(600000), avgCpuQuota)
	}
}

func TestNewTaskMetricWindowAverageAnalyzer(t *testing.T) {
	a := NewTaskMetricWindowAverageAnalyzer("task-1", 5, CpuQuota)
	assert.Equal(t, float64(0), a.(*TaskMetricWindowAverageAnalyzer).resultTask)
	assert.Equal(t, 5, a.(*TaskMetricWindowAverageAnalyzer).window)
	assert.Equal(t, "task-1", a.(*TaskMetricWindowAverageAnalyzer).taskId)
	assert.Equal(t, CpuQuota, a.(*TaskMetricWindowAverageAnalyzer).metricType)
}

func TestTaskMetricWindowAverageAnalyzer_VisitTaskMetricStore(t *testing.T) {
	tms := mockTaskMetricStore()
	// basic testing.
	assert.NotNil(t, tms)
	assert.NoError(t, taskMetricStoreTakeErr)
	for i := 1; i <= 5; i++ {
		analyzer := NewTaskMetricWindowAverageAnalyzer(fmt.Sprintf("task-%d", i), 5, CpuQuota)
		assert.Equal(t, float64(0), analyzer.(*TaskMetricWindowAverageAnalyzer).resultTask)
		tms.Accept(analyzer)
		result := analyzer.(*TaskMetricWindowAverageAnalyzer).GetResult()
		assert.Equal(t, float64(600000), result)
	}
}
