package metrics

import (
	"container/ring"
	"sync"
)

type Store interface {
	// Accept an Analyzer to be run on the stored metric data.
	Accept(Analyzer)
}

// HostMetricStore is a Store and stores host metrics.
type HostMetricStore struct {
	// amount of historic information to be maintained per metric.
	// defines the number of historic values.
	historyCap int
	// Host metrics.
	Data map[Type]*ring.Ring
	mu   sync.Mutex
}

type TaskMetricStore struct {
	// amount of historic information to be maintained per metric.
	// defines the number of historic values.
	historyCap int
	// Task metrics for each running task on host.
	Data map[string]map[Type]*ring.Ring
	mu   sync.Mutex
}

func NewHostMetricStore(capacity int) Store {
	return &HostMetricStore{
		historyCap: capacity,
		Data:       make(map[Type]*ring.Ring),
	}
}

func NewTaskMetricStore(capacity int) Store {
	return &TaskMetricStore{
		historyCap: capacity,
		Data:       make(map[string]map[Type]*ring.Ring),
	}
}

func (s *HostMetricStore) Take(t Type, val float64) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !t.isValid() {
		return NewInvalidMetricError("invalid host metric type specified")
	}
	if _, ok := s.Data[t]; !ok {
		s.Data[t] = ring.New(s.historyCap)
	}
	s.Data[t].Value = val
	s.Data[t] = s.Data[t].Next()
	return nil
}

func (s *HostMetricStore) Accept(analyzer Analyzer) {
	s.mu.Lock()
	defer s.mu.Unlock()
	analyzer.VisitHostMetricStore(s)
}

// Latest returns the most recent data point recorded for the metric type.
// Throws an error if no data point available.
func (s *HostMetricStore) Latest(t Type) (float64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !t.isValid() {
		return 0, NewInvalidMetricError("invalid metric type provided")
	}
	if data, ok := s.Data[t]; !ok {
		return 0, NewNoDataForMetricError("no data found for metric type")
	} else {
		// data would be pointing to one location ahead of the last value inserted.
		return data.Prev().Value.(float64), nil
	}
}

// LatestN returns at most past N data points recorded for the metric type.
// Throws an error if no data point available for the specified type.
func (s *HostMetricStore) LatestN(t Type, n int) ([]float64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !t.isValid() {
		return nil, NewInvalidMetricError("invalid metric type provided")
	}
	var result []float64
	if metricData, ok := s.Data[t]; !ok {
		return nil, NewNoDataForMetricError("no data found for metric type")
	} else {
		cur := metricData.Prev()
		// We can at max return capacity number of data points.
		size := metricData.Len()
		if n < metricData.Len() {
			size = n
		}
		for i := 0; (i < size) && (cur.Value != nil); i++ {
			result = append([]float64{cur.Value.(float64)}, result...)
			cur = cur.Prev()
		}
	}
	return result, nil
}

func (s *TaskMetricStore) Take(taskId string, t Type, val float64) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !t.isValid() {
		return NewInvalidMetricError("invalid metric type specified")
	}
	if _, ok := s.Data[taskId]; !ok {
		s.Data[taskId] = make(map[Type]*ring.Ring)
	}
	if _, ok := s.Data[taskId][t]; !ok {
		s.Data[taskId][t] = ring.New(s.historyCap)
	}
	s.Data[taskId][t].Value = val
	s.Data[taskId][t] = s.Data[taskId][t].Next()
	return nil
}

func (s *TaskMetricStore) TaskTerminated(taskId string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.Data, taskId)
}

func (s *TaskMetricStore) Accept(analyzer Analyzer) {
	s.mu.Lock()
	defer s.mu.Unlock()
	analyzer.VisitTaskMetricStore(s)
}

// Latest returns the most recent data point recorded for the task and metric type.
// Throws an error if either taskId is invalid or no data point available.
func (s *TaskMetricStore) Latest(taskId string, t Type) (float64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !t.isValid() {
		return 0, NewInvalidMetricError("invalid metric type specified")
	}
	if data, ok := s.Data[taskId]; !ok {
		return 0, NewInvalidTaskIdError("invalid taskId")
	} else if metricData, ok := data[t]; !ok {
		return 0, NewNoDataForMetricError("no data point recorded for metric")
	} else {
		// data would be pointing to one location ahead of the last value inserted.
		return metricData.Prev().Value.(float64), nil
	}
}

// LatestN returns at most past N data points recorded for the metric type.
// Throws an error if no data point available for the specified type.
func (s *TaskMetricStore) LatestN(taskId string, t Type, n int) ([]float64, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	var result []float64
	if !t.isValid() {
		return nil, NewInvalidMetricError("invalid metric type specified")
	}
	if data, ok := s.Data[taskId]; !ok {
		return nil, NewInvalidTaskIdError("invalid taskId")
	} else if metricData, ok := data[t]; !ok {
		return nil, NewNoDataForMetricError("no data point recorded for metric")
	} else {
		// data would be pointing to one location ahead of the last value inserted.
		cur := metricData.Prev()
		// We can at max return capacity number of data points.
		size := metricData.Len()
		if n < metricData.Len() {
			size = n
		}
		for i := 0; (i < size) && (cur.Value != nil); i++ {
			result = append([]float64{cur.Value.(float64)}, result...)
			cur = cur.Prev()
		}
		return result, nil
	}
}

// InvalidMetricError is an error for an invalid metric.
type InvalidMetricError struct {
	msg string
}

func NewInvalidMetricError(msg string) error {
	return &InvalidMetricError{msg: msg}
}

func (e *InvalidMetricError) Error() string {
	return e.msg
}

// NoDataForMetricError is an error for no data available for provided metric.
type NoDataForMetricError struct {
	msg string
}

func NewNoDataForMetricError(msg string) error {
	return &NoDataForMetricError{msg: msg}
}

func (e *NoDataForMetricError) Error() string {
	return e.msg
}

// InvalidTaskIdError is an error for no data available for provided metric.
type InvalidTaskIdError struct {
	msg string
}

func NewInvalidTaskIdError(msg string) error {
	return &InvalidTaskIdError{msg: msg}
}

func (e *InvalidTaskIdError) Error() string {
	return e.msg
}
