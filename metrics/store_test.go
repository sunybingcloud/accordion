package metrics

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

var hostMetricStore Store
var hostMetricStoreTakeErr error
var taskMetricStore Store
var taskMetricStoreTakeErr error

const capacity = 5

func TestMain(m *testing.M) {
	hostMetricStore = mockHostMetricStore()
	taskMetricStore = mockTaskMetricStore()
	m.Run()
}

func mockHostMetricStore() Store {
	s := NewHostMetricStore(capacity)
	for i := 100; i < 105; i++ {
		hostMetricStoreTakeErr = s.(*HostMetricStore).Take(CpuPower, float64(i))
		if hostMetricStoreTakeErr != nil {
			break // preserving the error
		}
	}
	return s
}

func mockTaskMetricStore() Store {
	s := NewTaskMetricStore(capacity)
	for i := 1; i <= 5; i++ {
		for j := 200000; j <= 1000000; j = j + 200000 {
			taskMetricStoreTakeErr = s.(*TaskMetricStore).Take(fmt.Sprintf("task-%d", i),
				CpuQuota, float64(j))
			if taskMetricStoreTakeErr != nil {
				break // preserving the error
			}
		}
	}
	return s
}

func TestNewHostMetricStore(t *testing.T) {
	assert.NotNil(t, hostMetricStore)
	assert.NoError(t, hostMetricStoreTakeErr)
	assert.Len(t, hostMetricStore.(*HostMetricStore).Data, 1)
	powerData, ok := hostMetricStore.(*HostMetricStore).Data[CpuPower]
	assert.True(t, ok)
	assert.Equal(t, powerData.Len(), capacity)
	for i := 100; i < 105; i++ {
		assert.Equal(t, float64(i), powerData.Value)
		powerData = powerData.Next()
	}
}

func TestNewTaskMetricStore(t *testing.T) {
	assert.NotNil(t, taskMetricStore)
	assert.NoError(t, taskMetricStoreTakeErr)
	assert.Len(t, taskMetricStore.(*TaskMetricStore).Data, 5)
	for i := 1; i <= 5; i++ {
		taskMetrics, ok := taskMetricStore.(*TaskMetricStore).Data[fmt.Sprintf("task-%d", i)]
		assert.True(t, ok)
		cpuQuotaData, ok := taskMetrics[CpuQuota]
		assert.True(t, ok)
		assert.Equal(t, cpuQuotaData.Len(), capacity)
		for j := 200000; j < 1000000; j = j + 200000 {
			assert.Equal(t, float64(j), cpuQuotaData.Value)
			cpuQuotaData = cpuQuotaData.Next()
		}
	}
}

func TestHostMetricStore_Latest(t *testing.T) {
	t.Run("invalid metric type", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		_, err := hms.(*HostMetricStore).Latest(Type(-1))
		assert.Error(t, err)
	})

	t.Run("no data point available for valid metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		_, err := hms.(*HostMetricStore).Latest(CpuQuota)
		assert.Error(t, err)
	})

	t.Run("data point available for valid metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := hms.(*HostMetricStore).Latest(CpuPower)
		assert.NoError(t, err)
		assert.Equal(t, float64(500), data)
	})
}

func TestHostMetricStore_LatestN(t *testing.T) {
	t.Run("invalid metric type and n=capacity", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		_, err := hms.(*HostMetricStore).LatestN(Type(-1), capacity)
		assert.Error(t, err)
	})

	t.Run("n=capacity and no data point available for metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		_, err := hms.(*HostMetricStore).LatestN(CpuQuota, capacity)
		assert.Error(t, err)
	})

	t.Run("n=capacity and data points available for metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := hms.(*HostMetricStore).LatestN(CpuPower, capacity)
		assert.NoError(t, err)
		assert.Len(t, data, capacity)
		assert.ElementsMatch(t, []float64{100, 200, 300, 400, 500}, data)
		// checking order as well.
		for i := 0; i < capacity; i++ {
			assert.Equal(t, 100*float64(i+1), data[i])
		}
	})

	t.Run("n=capacity and capacity-1 data points available for metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity-1; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := hms.(*HostMetricStore).LatestN(CpuPower, capacity)
		assert.NoError(t, err)
		assert.Len(t, data, capacity-1)
		assert.ElementsMatch(t, []float64{100, 200, 300, 400}, data)
		// checking order as well.
		for i := 0; i < capacity-1; i++ {
			assert.Equal(t, 100*float64(i+1), data[i])
		}
	})

	t.Run("n>capacity and capacity data points available for metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := hms.(*HostMetricStore).LatestN(CpuPower, capacity+1)
		assert.NoError(t, err)
		assert.Len(t, data, capacity)
		assert.ElementsMatch(t, []float64{100, 200, 300, 400, 500}, data)
		// checking order as well.
		for i := 0; i < capacity; i++ {
			assert.Equal(t, 100*float64(i+1), data[i])
		}
	})

	t.Run("n>capacity and capacity-1 data points available for metric", func(t *testing.T) {
		hms := NewHostMetricStore(capacity)
		for i := 0; i < capacity-1; i++ {
			err := hms.(*HostMetricStore).Take(CpuPower, 100*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := hms.(*HostMetricStore).LatestN(CpuPower, capacity+1)
		assert.NoError(t, err)
		assert.Len(t, data, capacity-1)
		assert.ElementsMatch(t, []float64{100, 200, 300, 400}, data)
		// checking order as well.
		for i := 0; i < capacity-1; i++ {
			assert.Equal(t, 100*float64(i+1), data[i])
		}
	})
}

func TestTaskMetricStore_Latest(t *testing.T) {
	t.Run("invalid taskId", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		_, err := tms.(*TaskMetricStore).Latest("", CpuQuota)
		assert.Error(t, err)
	})

	t.Run("invalid metric type", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000)
		assert.NoError(t, err)
		_, err = tms.(*TaskMetricStore).Latest("t1", Type(-1))
		assert.Error(t, err)
	})

	t.Run("no data point available for valid metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		_, err := tms.(*TaskMetricStore).Latest("t1", CpuPower)
		assert.Error(t, err)
	})

	t.Run("data point available for valid metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := tms.(*TaskMetricStore).Latest("t1", CpuQuota)
		assert.NoError(t, err)
		assert.Equal(t, float64(500000), data)
	})
}

func TestTaskMetricStore_LatestN(t *testing.T) {
	t.Run("invalid taskId and n=capacity", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		_, err := tms.(*TaskMetricStore).LatestN("t1", CpuQuota, capacity)
		assert.Error(t, err)
	})

	t.Run("invalid metric type and n=capacity", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000)
		assert.NoError(t, err)
		_, err = tms.(*TaskMetricStore).LatestN("t1", Type(-1), capacity)
		assert.Error(t, err)
	})

	t.Run("n=capacity and no data point available for valid metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		_, err := tms.(*TaskMetricStore).LatestN("t1", CpuShares, capacity)
		assert.Error(t, err)
	})

	t.Run("n=capacity and data points available for valid metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := tms.(*TaskMetricStore).LatestN("t1", CpuQuota, capacity)
		assert.NoError(t, err)
		assert.Len(t, data, capacity)
		assert.ElementsMatch(t, []float64{100000, 200000, 300000, 400000, 500000}, data)
		// checking order as well.
		for i := 0; i < capacity; i++ {
			assert.Equal(t, 100000*float64(i+1), data[i])
		}
	})

	t.Run("n=capacity and capacity-1 data points available for metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity-1; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := tms.(*TaskMetricStore).LatestN("t1", CpuQuota, capacity)
		assert.NoError(t, err)
		assert.Len(t, data, capacity-1)
		assert.ElementsMatch(t, []float64{100000, 200000, 300000, 400000}, data)
		// checking order as well.
		for i := 0; i < capacity-1; i++ {
			assert.Equal(t, 100000*float64(i+1), data[i])
		}
	})

	t.Run("n>capacity and capacity data points available for metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := tms.(*TaskMetricStore).LatestN("t1", CpuQuota, capacity+1)
		assert.NoError(t, err)
		assert.Len(t, data, capacity)
		assert.ElementsMatch(t, []float64{100000, 200000, 300000, 400000, 500000}, data)
		// checking order as well.
		for i := 0; i < capacity; i++ {
			assert.Equal(t, 100000*float64(i+1), data[i])
		}
	})

	t.Run("n>capacity and capacity-1 data points available for metric", func(t *testing.T) {
		tms := NewTaskMetricStore(capacity)
		for i := 0; i < capacity-1; i++ {
			err := tms.(*TaskMetricStore).Take("t1", CpuQuota, 100000*float64(i+1))
			assert.NoError(t, err)
		}
		data, err := tms.(*TaskMetricStore).LatestN("t1", CpuQuota, capacity+1)
		assert.NoError(t, err)
		assert.Len(t, data, capacity-1)
		assert.ElementsMatch(t, []float64{100000, 200000, 300000, 400000}, data)
		// checking order as well.
		for i := 0; i < capacity-1; i++ {
			assert.Equal(t, 100000*float64(i+1), data[i])
		}
	})
}
