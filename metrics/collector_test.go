package metrics

import (
	"fmt"
	"github.com/pradykaushik/task-ranker/entities"
	"github.com/stretchr/testify/assert"
	"testing"
)

var collector *Collector

func TestNewCollector(t *testing.T) {
	hms := NewHostMetricStore(5)
	tms := NewTaskMetricStore(5)
	collector = NewCollector(hms, tms)
	assert.NotNil(t, collector.hostMetricStore)
	assert.NotNil(t, collector.taskMetricStore)
}

func TestCollector_ReceivePowerUsage(t *testing.T) {
	for i := 100; i < 105; i++ {
		collector.ReceivePowerUsage(CpuPower, float64(i))
	}

	assert.Len(t, collector.hostMetricStore.(*HostMetricStore).Data, 1)
	powerData, ok := collector.hostMetricStore.(*HostMetricStore).Data[CpuPower]
	assert.True(t, ok)
	assert.Equal(t, powerData.Len(), capacity)
	for i := 100; i < 105; i++ {
		assert.Equal(t, float64(i), powerData.Value)
		powerData = powerData.Next()
	}
}

func TestCollector_Receive(t *testing.T) {
	// mocking taskMetrics as would be received from task ranker.
	for i := 200000; i <= 1000000; i = i + 200000 {
		taskMetrics := make(entities.RankedTasks)
		for j := 1; j <= 5; j++ {
			taskId := fmt.Sprintf("task-%d", j)
			taskMetrics[entities.Hostname(taskId)] = []entities.Task{
				{
					ID:     cpuQuotaString,
					Weight: float64(i),
				},
				{
					ID:     cpuUtilizationString,
					Weight: float64(i / 1000),
				},
			}
		}
		collector.Receive(taskMetrics)
	}

	// testing whether the metrics have been correctly stored.
	assert.Len(t, collector.taskMetricStore.(*TaskMetricStore).Data, 5)
	for i := 1; i <= 5; i++ {
		taskId := fmt.Sprintf("task-%d", i)
		taskMetrics, ok := collector.taskMetricStore.(*TaskMetricStore).Data[taskId]
		assert.True(t, ok)
		cpuQuotaData, ok := taskMetrics[CpuQuota]
		assert.True(t, ok)
		cpuUtilData, ok := taskMetrics[CpuUtilization]
		assert.True(t, ok)
		assert.Equal(t, cpuQuotaData.Len(), capacity)
		assert.Equal(t, cpuUtilData.Len(), capacity)
		for j := 200000; j < 1000000; j = j + 200000 {
			assert.Equal(t, float64(j), cpuQuotaData.Value)
			assert.Equal(t, float64(j/1000), cpuUtilData.Value)
			cpuQuotaData = cpuQuotaData.Next()
			cpuUtilData = cpuUtilData.Next()
		}
	}
}
