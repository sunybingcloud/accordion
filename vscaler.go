package main

import (
	"bitbucket.org/sunybingcloud/accordion/def"
	aLog "bitbucket.org/sunybingcloud/accordion/logging"
	"bitbucket.org/sunybingcloud/accordion/metrics"
	"container/heap"
	"context"
	"fmt"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"
	"github.com/sajari/regression"
	"github.com/sirupsen/logrus"
	"log"
	"math"
	"runtime"
	"strconv"
	"sync"
	"time"
)

// VScaler is a vertical scaler.
// Implements cron.Job interface and therefore can be run as a cron job.
type VScaler struct {
	recommender      VScalerRecommender
	resourceUpdater  VScalerResourceUpdater
	runningTasks     map[TaskId]*runningTaskInfo
	scaleLimitCpuMax int64
	scaleLimitCpuMin int64
	scalingSchedule  cron.Schedule
	// Interpreted as the #scaling_cycles to be used for profiling.
	profilingWindow int64
	// periodicProfiling indicates whether applications need to be profiled periodically.
	// If set to true, then profilingIntervalCycles determines the frequency in # scaling cycles.
	periodicProfiling       bool
	profilingIntervalCycles int64

	runner *cron.Cron
	mu     sync.Mutex
}

type runningTaskInfo struct {
	cpuQuotaAllocation int64
	containerId        ContainerId
	submittedTime      time.Time
	// #seconds between submitted time and deadline.
	totalAvailableTime int64
}

// VScalerRecommender defines an API for scaling recommenders.
type VScalerRecommender interface {
	// Initialize the recommender.
	Init(metricSchedule cron.Schedule, hostMetricStore metrics.Store, taskMetricStore metrics.Store, backlogPressure float64)
	// Execute the scaling recommender that returns multiple recommendations of resource adjustments for each task.
	Execute(scaler *VScaler) map[TaskId][]int64
}

type VScalerResourceUpdater interface {
	// Initialize the resource updater.
	Init(hostMetricStore metrics.Store, taskMetricStore metrics.Store, client *client.Client, ctx context.Context)
	// Execute the scaling recommendation  that picks one from the provided
	// list of recommended resource adjustment options.
	Execute(runningTasks map[TaskId]*runningTaskInfo, recommendations map[TaskId][]int64)
}

// Defining types for task id and container id to avoid confusion when mapping as both are strings.
type TaskId string
type ContainerId string

func (id TaskId) Value() string {
	return string(id)
}

func (id ContainerId) Value() string {
	return string(id)
}

// VScalerOption represents configuration option for a VScaler object.
type VScalerOption func(*VScaler) error

// WithRecommender returns a VScalerOption that sets the scaling recommender to the provided one.
func WithRecommender(r VScalerRecommender) VScalerOption {
	return func(s *VScaler) error {
		if r == nil {
			return errors.New("nil scaling recommender provided")
		}
		s.recommender = r
		return nil
	}
}

// WithResourceUpdater returns a VScalerOption that sets the scaling recommender to the provided one.
func WithResourceUpdater(rs VScalerResourceUpdater) VScalerOption {
	return func(s *VScaler) error {
		if rs == nil {
			return errors.New("nil recommendation  provided")
		}
		s.resourceUpdater = rs
		return nil
	}
}

// WithScalerLimits returns a VScalerOption that sets the scaling limits to the provided ones.
func WithScalingLimits(max int64, min int64) VScalerOption {
	return func(s *VScaler) error {
		ncpus := int64(runtime.NumCPU())
		if max > ncpus {
			return errors.New("max scale limit > #cpus available")
		}
		if min < 1 {
			return errors.New("min scale limit < 1")
		}

		s.scaleLimitCpuMax = max
		s.scaleLimitCpuMin = min
		return nil
	}
}

// WithSchedule returns a VScalerOption that sets the schedule on which the scaler is to be run.
func WithSchedule(sched string) VScalerOption {
	return func(s *VScaler) error {
		var scalingSchedule cron.Schedule
		var err error
		if sched == "" {
			err = errors.New("invalid cron schedule provided for scaler")
		} else {
			scalingSchedule, err = cron.NewParser(
				cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(sched)
			if err != nil {
				err = errors.Wrap(err, "invalid cron schedule provided for scaler")
			}
		}
		s.scalingSchedule = scalingSchedule
		return err
	}
}

// WithProfilingWindow returns a VScalerOption that sets the size of the profiling window.
func WithProfilingWindow(n int64) VScalerOption {
	return func(s *VScaler) error {
		if n < 1 {
			return errors.New("invalid profiling window provided")
		}
		s.profilingWindow = n
		return nil
	}
}

// WithPeriodicProfiling returns a VScalerOption that sets the periodic profiling options.
// An error is returned if profilingIntervalCycles <= 0.
func WithPeriodicProfiling(periodicProfiling bool, profilingIntervalCycles int64) VScalerOption {
	return func(s *VScaler) error {
		if !periodicProfiling {
			return nil
		}
		if profilingIntervalCycles <= 0 {
			return errors.New("invalid profiling interval: got <= 0, want > 0")
		}
		s.periodicProfiling = periodicProfiling
		s.profilingIntervalCycles = profilingIntervalCycles
		return nil
	}
}

// NewVScaler returns a new VScaler configured using the provided options.
func NewVScaler(opts ...VScalerOption) (*VScaler, error) {
	s := new(VScaler)
	s.runningTasks = make(map[TaskId]*runningTaskInfo)
	for _, opt := range opts {
		if err := opt(s); err != nil {
			return nil, errors.Wrap(err, "failed to instantiate VScaler object")
		}
	}
	// Checking to see if all the required fields have been initialized.
	if s.scalingSchedule == nil {
		return nil, errors.New("scaling schedule not initialized")
	}
	if s.scaleLimitCpuMax == 0 {
		return nil, errors.New("max scaling limit not specified")
	}
	if s.scaleLimitCpuMin == 0 {
		return nil, errors.New("min scaling limit not specified")
	}
	if s.recommender == nil {
		return nil, errors.New("scaling recommender not initialized")
	}
	if s.resourceUpdater == nil {
		return nil, errors.New("resource updater not initialized")
	}
	if s.profilingWindow == 0 {
		return nil, errors.New("profiling window not initialized")
	}
	if s.periodicProfiling && (s.profilingIntervalCycles <= 0) {
		return nil, fmt.Errorf("invalid profiling interval: got %d, want > 0", s.profilingIntervalCycles)
	}

	return s, nil
}

func (s *VScaler) Run() {
	s.mu.Lock()
	defer s.mu.Unlock()
	// execute recommender.
	// select resource adjustment for each task.
	// update the resource allocation of the corresponding containers.
	recommendations := s.recommender.Execute(s)
	s.resourceUpdater.Execute(s.runningTasks, recommendations)
}

func (s *VScaler) Start() {
	s.runner = cron.New(cron.WithSeconds())
	s.runner.Schedule(s.scalingSchedule, s)
	s.runner.Start()
}

func (s *VScaler) Stop() {
	s.runner.Stop()
}

// NewTask accepts a newly launched task.
func (s *VScaler) NewTask(t def.Task, taskId string, containerId string, cpuquota int64) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, ok := s.runningTasks[TaskId(taskId)]; ok {
		return
	}
	s.runningTasks[TaskId(taskId)] = &runningTaskInfo{
		cpuQuotaAllocation: cpuquota,
		containerId:        ContainerId(containerId),
		submittedTime:      t.SubmittedTime,
		totalAvailableTime: t.TimeToDeadline,
	}
}

func (s *VScaler) TaskTerminated(taskId string) {
	s.mu.Lock()
	defer s.mu.Unlock()
	delete(s.runningTasks, TaskId(taskId))
}

// NaiveVScalerRecommender is a naive implementation of a recommender.
type NaiveVScalerRecommender struct {
	metricSchedule  cron.Schedule
	hostMetricStore metrics.Store
	taskMetricStore metrics.Store
}

// Initialize the recommender.
func (r *NaiveVScalerRecommender) Init(
	metricSchedule cron.Schedule,
	hostMetricStore, taskMetricStore metrics.Store,
	_ float64) {

	r.metricSchedule = metricSchedule
	r.hostMetricStore = hostMetricStore
	r.taskMetricStore = taskMetricStore
}

func oneCPU() int64 {
	return 1 * CpuPeriod
}

func nCPU(n int64) int64 {
	return n * CpuPeriod
}

// Execute returns scaling recommendations +1/-1 from the current cpu allocation.
func (r *NaiveVScalerRecommender) Execute(scaler *VScaler) map[TaskId][]int64 {

	var recommendations = make(map[TaskId][]int64)
	for taskId, rtInfo := range scaler.runningTasks {
		cpu, err := r.taskMetricStore.(*metrics.TaskMetricStore).Latest(string(taskId), metrics.CpuQuota)
		if err != nil {
			switch err.(type) {
			case *metrics.InvalidTaskIdError:
				// Task no longer running.
			case *metrics.NoDataForMetricError, *metrics.InvalidMetricError:
				// shouldn't be here.
			}
			continue
		} else {
			min := scaler.scaleLimitCpuMin * CpuPeriod
			oneLessCpu := int64(cpu) - oneCPU()
			if oneLessCpu > min {
				min = oneLessCpu
			}

			max := scaler.scaleLimitCpuMax * CpuPeriod
			oneMoreCpu := int64(cpu) + oneCPU()
			if oneMoreCpu < max {
				max = oneMoreCpu
			}

			recommendations[taskId] = []int64{min, max}
			now := time.Now()
			r.Log(string(taskId), string(rtInfo.containerId), now, max, min)
		}
	}
	return recommendations
}

func (*NaiveVScalerRecommender) Log(
	taskId string,
	containerId string,
	now time.Time,
	cpuMax, cpuMin int64) {
	aLog.WithFields(logrus.Fields{
		"recommender":            "naive",
		"timestamp":              strconv.FormatInt(now.UnixNano(), 10),
		"taskId":                 taskId,
		"containerId":            containerId,
		"max_cpu_recommendation": strconv.FormatInt(cpuMax, 10),
		"min_cpu_recommendation": strconv.FormatInt(cpuMin, 10),
	}).Log(logrus.InfoLevel)
}

// COMMENT
// AvgIRRBasedVScalerRecommender recommends resource adjustments to applications based on the net impact
// that the applications have suffered.
//
// Impact is defined as a significant reduction in average of instruction retirement rate (IRR) over the window of time.
// The number of data points within the window = rate(scaling) / rate(metrics collected)
// So, if the scaler is run once every minute and metrics are collected every 5 seconds, then #data points in window = 12.
//
// The recommendations include a range [max, min] such that any value in the range, inclusive, can be selected by
// the resource updater. The recommender monitors the application for a defined number of cycles. This is done in order
// to estimate how much suffering is accepted.
// Currently, if avg_irr = X, then total suffering = 0.1 * X. A bucket is then created of this size.
// Every -ve impact is added to the bucket and every +ve impact is subtracted from the bucket.
//
// If the bucket is full, then the only option is to scale up as there is no more room to suffer.
// If the bucket is empty, then the only option is to scale down as we have not suffered at all.
// 		Note that our ultimate goal is to make smart trade-offs between power and performance.
// When the bucket is partially filled, then we can either scale up or scale down.
// To estimate the extent to which we can scale up/down,
// 		1. we estimate the min cpuquota such that estimated impact <= room in bucket.
// 		2. we estimate the max cpuquota such that estimate +ve impact is as close to size of bucket (impact suffered).
//
// Impact estimates is performed using partial derivatives.
// To PK - Refer https://docs.google.com/spreadsheets/d/0B0Psr1-HrErBX01ld182Q09uT0ljQ0k0cFhveUtkOWwwSWFz/edit#gid=549837801.
// END COMMENT
type AvgIRRBasedVScalerRecommender struct {
	scalingScheduleSeconds    int64
	metricSchedule            cron.Schedule
	metricScheduleSeconds     int64
	hostMetricStore           metrics.Store
	taskMetricStore           metrics.Store
	metadata                  map[TaskId]*metadataAvgIRRBasedScaling
	scalingScheduleRecordOnce sync.Once
	bklPressure               float64
}

type metadataAvgIRRBasedScaling struct {
	bkl *Backlog
	// information pertaining to profiling.
	profiling *Profiling
	// cyclesUntilProfiling is a decrementing counter that stores the number of cycles
	// remaining until the next profiling stage.
	// By default, cyclesUntilProfiling = -1 (periodicProfiling = false).
	cyclesUntilProfiling    int64
	profilingIntervalCycles int64
	// keeping track of this as we will never scale beyond the requested quota.
	// this is assuming that the resources are over estimated, which is the norm.
	requestedCpuQuotaAllocation           int64
	requestedCpuQuotaAllocationRecordOnce sync.Once
	irrRegression                         *regression.Regression
	regressionDataPoints                  regression.DataPoints
	remainingTTDSeconds                   int64
}

func (m *metadataAvgIRRBasedScaling) String() string {
	return fmt.Sprintf("{curBkl[value=%d, avgIrrEstNoScale=%f], profiling[inProgress=%v, cyclesRem=%d], "+
		"reqCpuAllocation=%d, remainingTTDSeconds=%d, cyclesUntilProfiling=%d}",
		m.bkl.value, m.bkl.avgIrrEstNoScale, m.profiling.inProgress, m.profiling.cyclesRemaining,
		m.requestedCpuQuotaAllocation, m.remainingTTDSeconds, m.cyclesUntilProfiling)
}

func newMetadataAvgIRRBasedScaling() *metadataAvgIRRBasedScaling {
	return &metadataAvgIRRBasedScaling{
		bkl:                                   nil,
		profiling:                             nil,
		cyclesUntilProfiling:                  -1,
		profilingIntervalCycles:               0,
		requestedCpuQuotaAllocation:           -1,
		requestedCpuQuotaAllocationRecordOnce: sync.Once{},
		irrRegression:                         nil,
		remainingTTDSeconds:                   -1,
	}
}

func (m *metadataAvgIRRBasedScaling) recordDataPoint(avgIrr float64, cpu int64) {
	m.regressionDataPoints = append(m.regressionDataPoints,
		regression.DataPoint(avgIrr, []float64{float64(cpu)}))
}

func (m *metadataAvgIRRBasedScaling) resetCyclesUntilProfiling() {
	m.cyclesUntilProfiling = m.profilingIntervalCycles
}

func (m *metadataAvgIRRBasedScaling) elapseCyclesUntilProfiling(cycles int64) {
	m.cyclesUntilProfiling -= cycles
}

func (m *metadataAvgIRRBasedScaling) timeToProfile() bool {
	return m.cyclesUntilProfiling == 0
}

func (r *AvgIRRBasedVScalerRecommender) Init(
	metricSchedule cron.Schedule,
	hostMetricStore, taskMetricStore metrics.Store,
	backlogPressure float64) {

	r.hostMetricStore = hostMetricStore
	r.taskMetricStore = taskMetricStore
	r.metadata = make(map[TaskId]*metadataAvgIRRBasedScaling)

	r.metricSchedule = metricSchedule
	now := time.Now()
	next := r.metricSchedule.Next(now)
	then := r.metricSchedule.Next(next)
	r.metricScheduleSeconds = int64(then.Sub(next).Seconds())
	r.bklPressure = backlogPressure
}

// ProfilingInfo encapsulates profiling related information.
type Profiling struct {
	inProgress      bool
	cyclesRemaining int64
	totalCycles     int64
}

func (p Profiling) IsInProgress() bool {
	return p.inProgress
}

func (p Profiling) HasFinished() bool {
	return p.cyclesRemaining <= 0
}

func (p *Profiling) Start(n int64) {
	p.totalCycles = n
	p.cyclesRemaining = n
	p.inProgress = true
	p.Do()
}

func (p *Profiling) Do() {
	p.cyclesRemaining -= 1
}

func (p *Profiling) Stop() {
	p.inProgress = false
}

// Backlog represents the instruction backlog accrued by an application.
type Backlog struct {
	// avgIrrEstNoScale represents the behavior of the application as the average instruction retirement rate
	// during the current phase of execution.
	avgIrrEstNoScale float64
	// value represents the number of instructions that are estimated to be pending execution.
	value int64
	// totalTimeTaken is the total time taken to execute all the previously accrued backlog.
	totalTimeTaken float64
	// total accrued backlog during the lifetime of the application.
	totalAccrued int64
}

func newBacklog() *Backlog {
	return &Backlog{
		avgIrrEstNoScale: 0,
		value:            0,
		totalTimeTaken:   0,
		totalAccrued:     0,
	}
}

// ExecTime returns the time needed to retire any pending instruction backlog assuming the current behavior of the application.
func (b *Backlog) ExecTime() float64 {
	// time taken to execute any remaining backlog is estimated considering the current behavior of the application.
	return math.Round((float64(b.value)/b.avgIrrEstNoScale)*10) / 10
}

// TotalExecTime returns the sum of the total time already spent consuming all previous backlogs and
// the time required to retire any pending instruction backlog.
// Time is rounded to one decimal place.
func (b *Backlog) TotalExecTime() float64 {
	return math.Round((b.totalTimeTaken+b.ExecTime())*10) / 10
}

// ConsumeAndAccrue consumes any previously accrued backlog and accrues any new backlog.
// This assumes that the reduction in average instruction retirement rate is due to the application being
// vertically scaled down relative to the cpu resources requested.
// Backlog is calculated as the difference in expected and actual number of instructions retired plus any instructions
// used to execute previous backlog.
func (b *Backlog) ConsumeAndAccrue(
	irrDataPoints []float64,
	metricSchedSeconds, scalingSchedSeconds int64,
	scaledDown bool) bool {
	var instrExecuted int64 = 0
	// determine the portion of previous backlog consumed and calculating the real time taken for the same.
	var consumedBacklog int64 = 0
	var leftToConsume int64 = b.value // current backlog initially left to consume.
	var timeTakenToConsumeBacklog float64 = 0
	var consumed = false
	for _, irr := range irrDataPoints {
		nRetired := int64(irr) * metricSchedSeconds
		instrExecuted += nRetired
		if leftToConsume > 0 {
			portionConsumed := int64(math.Min(float64(nRetired), float64(leftToConsume)))
			t := math.Round((float64(portionConsumed)/irr)*10) / 10
			if !math.IsNaN(t) {
				timeTakenToConsumeBacklog += t
				consumedBacklog += portionConsumed
				leftToConsume -= portionConsumed
			} else {
				// irr was most likely 0.
			}
		}
	}
	// some or all of the previous backlog could have been executed.
	// accumulating the real time taken to execute the corresponding instructions.
	b.totalTimeTaken += timeTakenToConsumeBacklog
	b.value -= consumedBacklog
	if consumedBacklog > 0 {
		consumed = true
	}
	// accruing any new backlog if the application was scaled down.
	if scaledDown {
		remainingInstr := instrExecuted - consumedBacklog
		expectedInstrExecuted := b.avgIrrEstNoScale * float64(scalingSchedSeconds)
		newBacklog := int64(math.Max(0, expectedInstrExecuted-float64(remainingInstr)))
		b.value += newBacklog
		// tracking total amount of backlog we have accrued thus far.
		b.totalAccrued += newBacklog
	} else {
		// we are either profiling or have allocated the requested cpu, in which case,
		// no amount of backlog is accrued.
	}
	return consumed
}

// EstimateExecTimeForThroughputChange estimates and returns the time required to execute the current and any
// newly added backlog if the instruction retirement rate (throughput) were to change.
func (b Backlog) EstimateExecTimeForThroughputChange(avgIrrEst float64, scalingSchedSeconds int64) float64 {
	estInstrExecuted := avgIrrEst * float64(scalingSchedSeconds)
	// some or all of the previous backlog could be executed.
	// estimating the time required to execute the corresponding instructions.
	consumedBacklog := int64(math.Min(estInstrExecuted, float64(b.value)))
	// accruing.
	remainingInstr := int64(estInstrExecuted) - consumedBacklog
	expectedInstrExecuted := b.avgIrrEstNoScale * float64(scalingSchedSeconds)
	b.value = (b.value - consumedBacklog) + int64(math.Max(0, expectedInstrExecuted-float64(remainingInstr)))
	// Calculating the time required to retire the new backlog assuming the estimated IRR (avgIrrEst).
	// The estimated IRR is chosen instead of the current behavior in order to account for the worst case scenario
	// where we never scale the application back up again.
	return math.Round((float64(b.value)/avgIrrEst)*10) / 10
}

func (r *AvgIRRBasedVScalerRecommender) updateAppBehavior(taskId TaskId, avgIrr float64) {
	if r.metadata[taskId].bkl == nil {
		r.metadata[taskId].bkl = newBacklog()
	}
	r.metadata[taskId].bkl.avgIrrEstNoScale = avgIrr
}

func (r *AvgIRRBasedVScalerRecommender) initRegressionModel(taskId TaskId) {
	reg := new(regression.Regression)
	reg.SetObserved("avgIrr")
	reg.SetVar(0, "cpu")
	reg.Train(regression.DataPoint(0, []float64{0})) // initial.
	r.metadata[taskId].irrRegression = reg
}

// behaviorPredictors stores predictors for each task that can be used to get predict the behavior for a
// given cpu allocation.
var behaviorPredictors map[TaskId]*regression.Regression

func (r *AvgIRRBasedVScalerRecommender) findCMaxMin(scaleLimitMin int64, taskId TaskId) (int64, int64) {
	// clearing predictedBehavior.
	// predictedBehavior = make(map[int64]float64)
	// clearing behaviorPredictors.
	behaviorPredictors = make(map[TaskId]*regression.Regression)
	m := r.metadata[taskId]
	m.irrRegression.Train(m.regressionDataPoints...)
	err := m.irrRegression.Run()
	if err == nil {
		behaviorPredictors[taskId] = m.irrRegression
	}
	var cMax, cMin = m.requestedCpuQuotaAllocation, scaleLimitMin
	isFirst := true
	found := false
	if err == nil {
		for cpuAdjustment := m.requestedCpuQuotaAllocation; cpuAdjustment >= scaleLimitMin; cpuAdjustment -= oneCPU() {
			predAvgIrr, _ := m.irrRegression.Predict([]float64{float64(cpuAdjustment)})
			if predAvgIrr >= 0 {
				// estimate execution time for current backlog + added backlog
				if isFirst {
					cMin = cpuAdjustment
					isFirst = false
					continue
				}
				totalBklExecTimeEst := m.bkl.EstimateExecTimeForThroughputChange(predAvgIrr, r.scalingScheduleSeconds)
				// Threshold Check.
				// spareTimeExists checks if there is spare time that can be used to accrue additional backlog.
				spareTimeExists := totalBklExecTimeEst < (r.bklPressure * float64(m.remainingTTDSeconds))
				if (totalBklExecTimeEst >= 0) && spareTimeExists {
					// cpu adjustment is valid.
					// this ensures that avgIrrEstimates only stores entries for valid cpu adjustments.
					found = true
					if cpuAdjustment < cMin { // no need to update cMax.
						cMin = cpuAdjustment
					}
				} else if totalBklExecTimeEst >= 0 { // spare time does not exist.
					// Scaling down cpu resources to anything <= cpuAdjustment would not leave
					// sufficient spare time to accrue backlog.
					// We stick with the max,min we have so far.
					break
				}
			}
		}

		if !found {
			// if here, then any cpu adjustment is assumed to be detrimental to application performance.
			// setting max and min to the requested cpu allocation.
			cMin = cMax
		}
	} else {
		// If here, then we were not able to train the regression model and therefore will be scaling
		// the application back up to the requested cpu allocation.
		log.Println("could not train regression model")
	}
	return cMax, cMin
}

// continueProfiling consumes any remaining backlog for the task, with the given id.
func (r *AvgIRRBasedVScalerRecommender) continueProfiling(taskId TaskId, m *metadataAvgIRRBasedScaling) {
	if m.bkl == nil {
		m.profiling.Do()
		log.Println("continuing to profile")
		return
	}
	numDataPoints := r.scalingScheduleSeconds / r.metricScheduleSeconds
	irrDataPoints, err := r.taskMetricStore.(*metrics.TaskMetricStore).LatestN(string(taskId),
		metrics.InstructionRetirementRate, int(numDataPoints))
	var consumed = false
	if err != nil {
		// shouldn't be here.
	} else {
		consumed = m.bkl.ConsumeAndAccrue(irrDataPoints, r.metricScheduleSeconds, r.scalingScheduleSeconds, false)
	}
	// if using this cycle to consume any backlog, then not counting it towards profiling.
	// note that if only part of this cycle was used to consume any remaining backlog, then we will
	// not be able to observe application behavior during the remaining portion of the cycle. That said,
	// we will not be causing any impact to application performance as the application will be allocated
	// the requested cpu during this time.
	//
	// reason: we want to profile to observe new application behavior.
	if consumed {
		log.Println("ignoring profiling cycle")
	} else {
		m.profiling.Do() // profile for another cycle.
		log.Println("continuing to profile")
	}
}

func (r *AvgIRRBasedVScalerRecommender) Execute(scaler *VScaler) map[TaskId][]int64 {
	// recording scaling intervals in seconds.
	r.scalingScheduleRecordOnce.Do(func() {
		now := time.Now()
		next := scaler.scalingSchedule.Next(now)
		then := scaler.scalingSchedule.Next(next)
		r.scalingScheduleSeconds = int64(then.Sub(next).Seconds())
	})

	var recommendations = make(map[TaskId][]int64)
	for taskId, rtInfo := range scaler.runningTasks {
		if _, ok := r.metadata[taskId]; !ok {
			r.metadata[taskId] = newMetadataAvgIRRBasedScaling()
			r.metadata[taskId].cyclesUntilProfiling = scaler.profilingIntervalCycles
			r.metadata[taskId].profilingIntervalCycles = scaler.profilingIntervalCycles
		}
		m := r.metadata[taskId]
		// recording remaining time to deadline for currently running applications.
		if m.remainingTTDSeconds < 0 {
			// first time.
			m.remainingTTDSeconds = rtInfo.totalAvailableTime - r.scalingScheduleSeconds
		} else {
			m.remainingTTDSeconds = m.remainingTTDSeconds - r.scalingScheduleSeconds
			if m.remainingTTDSeconds < 0 {
				m.remainingTTDSeconds = 0 // -ve does not make sense here.
			}
		}

		// recording requested cpu allocation once.
		m.requestedCpuQuotaAllocationRecordOnce.Do(func() {
			m.requestedCpuQuotaAllocation = rtInfo.cpuQuotaAllocation
		})

		// Need to reinitialize the regression object. Otherwise, there would be no change in the
		// predictions made. This is because, regression.Regression#Run() can be called only once.
		// For confirmation, see https://github.com/sajari/regression/issues/21.
		r.initRegressionModel(taskId)

		// initialize profiling information.
		if m.profiling == nil {
			m.profiling = &Profiling{
				inProgress:      true, // we start with profiling.
				cyclesRemaining: scaler.profilingWindow - 1,
				totalCycles:     scaler.profilingWindow,
			}
		}

		if m.profiling.IsInProgress() {
			if m.profiling.HasFinished() {
				// we should not have any backlog at this point.
				if (m.bkl == nil) || (m.bkl.value <= 0) {
					numDataPoints := (r.scalingScheduleSeconds / r.metricScheduleSeconds) * scaler.profilingWindow
					analyzer := metrics.NewTaskMetricWindowAverageAnalyzer(string(taskId), int(numDataPoints), metrics.InstructionRetirementRate)
					r.taskMetricStore.Accept(analyzer)
					avgIrr := analyzer.(*metrics.TaskMetricWindowAverageAnalyzer).GetResult()

					if !math.IsNaN(avgIrr) && (avgIrr != 0) {
						log.Println("profiling complete for ", taskId)
						r.updateAppBehavior(taskId, avgIrr)
						m.recordDataPoint(avgIrr, rtInfo.cpuQuotaAllocation)
						// Given that we do not have enough data points to analyze, we make a simple
						// recommendation of cmax = req cpu, cmin = cmax-2.
						cMin := nCPU(scaler.scaleLimitCpuMin)
						cpuMinRec := m.requestedCpuQuotaAllocation - nCPU(2)
						if cMin < cpuMinRec {
							cMin = cpuMinRec
						}
						recommendations[taskId] = []int64{m.requestedCpuQuotaAllocation, cMin}
						now := time.Now()
						r.Log(string(taskId), string(rtInfo.containerId), now, m.bkl, m.requestedCpuQuotaAllocation, cMin)
						// stop profiling.
						m.profiling.Stop()
						m.resetCyclesUntilProfiling()
					} else {
						// possible that no data point recorded.
						// not going to scale this application.
						// continuing to consume any remaining backlog before beginning to profile.
						r.continueProfiling(taskId, m)
					}
				} else {
					// continuing to consume any remaining backlog before beginning to profile.
					// here only if profiling window = 1 and we are attempting to re-profile.
					r.continueProfiling(taskId, m)
				}
			} else {
				// continuing to consume any remaining backlog before beginning to profile.
				r.continueProfiling(taskId, m)
			}
		} else {
			m.elapseCyclesUntilProfiling(1)
			irrDataPoints, err := r.taskMetricStore.(*metrics.TaskMetricStore).LatestN(string(taskId),
				metrics.InstructionRetirementRate, int(r.scalingScheduleSeconds))
			if err != nil {
				// shouldn't be here.
			} else {
				// accrue additional backlog, if any.
				if rtInfo.cpuQuotaAllocation < m.requestedCpuQuotaAllocation {
					m.bkl.ConsumeAndAccrue(irrDataPoints, r.metricScheduleSeconds, r.scalingScheduleSeconds, true)
				} else {
					m.bkl.ConsumeAndAccrue(irrDataPoints, r.metricScheduleSeconds, r.scalingScheduleSeconds, false)
				}
				log.Println("accrued backlog for ", taskId)
				analyzer := metrics.NewTaskMetricWindowAverageAnalyzer(string(taskId), int(r.scalingScheduleSeconds), metrics.InstructionRetirementRate)
				r.taskMetricStore.Accept(analyzer)
				avgIrr := analyzer.(*metrics.TaskMetricWindowAverageAnalyzer).GetResult()

				m.recordDataPoint(avgIrr, rtInfo.cpuQuotaAllocation)
				if m.timeToProfile() {
					m.resetCyclesUntilProfiling()
					m.profiling.Start(scaler.profilingWindow)
					log.Println("restarting profiling for ", taskId)
					// need to scale up all the way.
					cMax := m.requestedCpuQuotaAllocation
					cMin := m.requestedCpuQuotaAllocation
					recommendations[taskId] = []int64{cMax, cMin}
					now := time.Now()
					r.Log(string(taskId), string(rtInfo.containerId), now, m.bkl, cMax, cMin)
					continue
				}
				cMax, cMin := r.findCMaxMin(nCPU(scaler.scaleLimitCpuMin), taskId)
				recommendations[taskId] = []int64{cMax, cMin}
				now := time.Now()
				r.Log(string(taskId), string(rtInfo.containerId), now, m.bkl, cMax, cMin)
			}
		}
	}
	return recommendations
}

func (*AvgIRRBasedVScalerRecommender) Log(
	taskId string,
	containerId string,
	now time.Time,
	bkl *Backlog,
	cpuMax, cpuMin int64) {
	aLog.WithFields(logrus.Fields{
		"recommender":                     "avg-irr",
		"timestamp":                       strconv.FormatInt(now.UnixNano(), 10),
		"taskId":                          taskId,
		"containerId":                     containerId,
		"avgIrrEstNoScale":                strconv.FormatFloat(bkl.avgIrrEstNoScale, 'e', -1, 64),
		"currentBacklog_instructions":     strconv.FormatInt(bkl.value, 10),
		"currentBacklog_execTime_seconds": strconv.FormatFloat(bkl.ExecTime(), 'e', -1, 64),
		"totalBacklog_instructions":       strconv.FormatInt(bkl.totalAccrued, 10),
		"totalBacklog_execTime_seconds":   strconv.FormatFloat(bkl.TotalExecTime(), 'e', -1, 64),
		"max_cpu_recommendation":          strconv.FormatInt(cpuMax, 10),
		"min_cpu_recommendation":          strconv.FormatInt(cpuMin, 10),
	}).Log(logrus.InfoLevel)
}

// Recommender factory.
const (
	naiveRecommender       = "naive"
	avgIRRBasedRecommender = "avg_irr"
)

var recommenderFactory = map[string]VScalerRecommender{
	naiveRecommender:       &NaiveVScalerRecommender{},
	avgIRRBasedRecommender: &AvgIRRBasedVScalerRecommender{},
}

func GetVScalerRecommender(
	name string,
	metricSchedule cron.Schedule,
	hostMetricStore, taskMetricStore metrics.Store,
	backlogPressure float64) (VScalerRecommender, error) {

	var r VScalerRecommender
	var ok bool
	if r, ok = recommenderFactory[name]; !ok {
		return nil, errors.New("invalid recommender name provided")
	}
	if (hostMetricStore == nil) || (taskMetricStore == nil) {
		return nil, errors.New("valid metric stores need to be provided")
	}
	r.Init(metricSchedule, hostMetricStore, taskMetricStore, backlogPressure)
	return r, nil
}

// NaiveVScalerResourceUpdater is a naive implementation of a resource updater.
type NaiveVScalerResourceUpdater struct {
	hostMetricStore metrics.Store
	taskMetricStore metrics.Store
	dockerCli       *client.Client
	ctx             context.Context
}

// Initialize the resource updater.
func (u *NaiveVScalerResourceUpdater) Init(
	hostMetricStore, taskMetricStore metrics.Store,
	cli *client.Client, ctx context.Context) {

	u.hostMetricStore = hostMetricStore
	u.taskMetricStore = taskMetricStore
	u.dockerCli = cli
	u.ctx = ctx
}

// Execute the resource updater that selects and updates, from the recommendations, resources to allocate to the task.
// It is important to note that there is always a combination of recommendations that can be selected
// that sum up to the total available resources on the system.
//
// Let the time in seconds from Start to deadline be considered as total available time.
// The naive resource updater chooses to scale down an application if elapsed time < (0.5 * total available time).
// The naive resource updater chooses to scale up an application if elapsed time > (0.5 * total available time).
func (u *NaiveVScalerResourceUpdater) Execute(
	runningTasks map[TaskId]*runningTaskInfo,
	recommendations map[TaskId][]int64) {
	// for each recommendation, use the docker API and update the resource allocation.
	log.Println("scaling...")
	var toScaleUp = make(map[TaskId]int64)
	totalResources := int64(runtime.NumCPU()) * CpuPeriod
	var curTotalAllocatedResources int64 = 0
	var reclaimedResources int64 = 0 // resources reclaimed as a result of scaling down containers.
	// Scaling down.
	for taskId, rec := range recommendations {
		if info, ok := runningTasks[taskId]; !ok {
			// task could have terminated or not yet started.
			continue
		} else {
			curTotalAllocatedResources += info.cpuQuotaAllocation
			now := time.Now()
			elapsedTimeSeconds := int64(now.Sub(info.submittedTime).Seconds()) // rounding errors are fine.
			threshold := int64(0.5 * float64(info.totalAvailableTime))
			if elapsedTimeSeconds < threshold {
				_, err := u.dockerCli.ContainerUpdate(u.ctx, string(info.containerId), container.UpdateConfig{
					Resources: container.Resources{
						CPUQuota: rec[0],
					},
				})
				if err != nil {
					log.Printf("failed to scale down container[%s]", info.containerId)
				} else {
					u.Log(
						string(taskId), string(info.containerId), now,
						info.cpuQuotaAllocation, rec[0], "power")
					reclaimedResources += oneCPU()
					info.cpuQuotaAllocation = rec[0]
				}
			} else {
				// we need to scale up.
				toScaleUp[taskId] = rec[1]
			}
		}
	}

	// Scaling up if possible.
	totalRemainingResources := (totalResources - curTotalAllocatedResources) + reclaimedResources
	for taskId, adjustedCpuQuota := range toScaleUp {
		containerId := runningTasks[taskId].containerId
		if totalRemainingResources > 0 {
			_, err := u.dockerCli.ContainerUpdate(u.ctx, string(containerId), container.UpdateConfig{
				Resources: container.Resources{
					CPUQuota: int64(adjustedCpuQuota),
				},
			})
			if err != nil {
				log.Printf("failed to scale up container[%s]", containerId)
			} else {
				totalRemainingResources -= oneCPU()
				now := time.Now()
				u.Log(
					string(taskId), string(containerId), now,
					runningTasks[taskId].cpuQuotaAllocation, adjustedCpuQuota, "performance")
				runningTasks[taskId].cpuQuotaAllocation = adjustedCpuQuota
			}
		}
	}
}

func (*NaiveVScalerResourceUpdater) Log(
	taskId string,
	containerId string,
	now time.Time,
	curCpuQuota, newCpuQuota int64,
	bias string) {
	aLog.WithFields(logrus.Fields{
		"resource_updater":   "naive",
		"timestamp":          strconv.FormatInt(now.UnixNano(), 10),
		"taskId":             taskId,
		"containerId":        containerId,
		"current_cpu_quota":  strconv.FormatInt(curCpuQuota, 10),
		"adjusted_cpu_quota": strconv.FormatInt(newCpuQuota, 10),
		"bias":               bias,
	}).Log(logrus.InfoLevel)
}

// AvgIRRBasedVScalerResourceUpdater adjusts resource allocations of currently running applications by
// ranking them based on their remaining time to deadline.
// The cpu range [cpuMin, cpuMax] is divided into four sub-ranges and discretized into 5 values.
// sub-range 1 = [ cpuMin, 0.25 * (cpuMax-cpuMin) )
// sub-range 2 = [ cpuMin + (0.25 * (cpuMax-cpuMin)), cpuMin + (0.5 * (cpuMax-cpuMin)) )
// sub-range 3 = [ cpuMin + (0.5 * (cpuMax-cpuMin)), cpuMin + (0.75 * (cpuMax-cpuMin)) )
// sub-range 4 = [ cpuMin + (0.75 * (cpuMax-cpuMin)), cpuMax ]
// So, for example, if cpuMin=200000 and cpuMax=700000.
// sub-range 1 = [ 200000, 325000 )
// sub-range 2 = [ 325000, 450000 )
// sub-range 3 = [ 450000, 575000 )
// sub-range 4 = [ 575000, 700000 ]
// values = [200000, 325000, 450000, 575000, 700000]
// Applications that have elapsed
// 1. >= 80% of their total available time are scaled to values[4]
// 2. >= 60% and < 80% of their total available time are scaled to values[3]
// 3. >= 40% and < 60% of their total available time are scaled to values[2]
// 4. >= 20% and < 40% of their total available time are scaled to values[1].
// 5. < 20% of their total available time are scaled to values[0].
type AvgIRRBasedVScalerResourceUpdater struct {
	hostMetricStore metrics.Store
	taskMetricStore metrics.Store
	dockerCli       *client.Client
	ctx             context.Context
}

// recommendationItem encapsulates scaling recommendation information for a single application
// to provision for ranking.
type avgIrrRecommendationItem struct {
	taskId              TaskId
	remainingTTDSeconds int64
}

// Initialize the resource updater.
func (u *AvgIRRBasedVScalerResourceUpdater) Init(
	hostMetricStore, taskMetricStore metrics.Store,
	cli *client.Client, ctx context.Context) {

	u.hostMetricStore = hostMetricStore
	u.taskMetricStore = taskMetricStore
	u.dockerCli = cli
	u.ctx = ctx
}

// Implements heap.Interface.
type avgIrrRecommendations []*avgIrrRecommendationItem

func (ri avgIrrRecommendations) Len() int {
	return len(ri)
}

func (ri avgIrrRecommendations) Swap(i, j int) {
	ri[i], ri[j] = ri[j], ri[i]
}

func (ri avgIrrRecommendations) Less(i, j int) bool {
	return ri[i].remainingTTDSeconds < ri[j].remainingTTDSeconds
}

func (ri *avgIrrRecommendations) Push(item interface{}) {
	*ri = append(*ri, item.(*avgIrrRecommendationItem))
}

func (ri *avgIrrRecommendations) Pop() interface{} {
	n := len(*ri)
	item := (*ri)[n-1]
	(*ri)[n-1] = nil // prevent memory leak.
	*ri = (*ri)[:n-1]
	return item
}

// rank the provided scaling recommendations and return the rankings along with the
// cpu adjustment values to select.
func (u *AvgIRRBasedVScalerResourceUpdater) rank(
	runningTasksInfo map[TaskId]*runningTaskInfo,
	recommendations map[TaskId][]int64) (avgIrrRecommendations, map[TaskId][]int64) {

	var rankedTasks avgIrrRecommendations
	cpuAdjustmentValues := make(map[TaskId][]int64)
	now := time.Now()
	for taskId, cpuMaxMin := range recommendations {
		// check to see if the task is still running.
		if _, ok := runningTasksInfo[taskId]; !ok {
			continue // task has terminated.
		}
		// if cpuMax = cpuMin, then we have no choice but to allocate the recommended cpu.
		// all the cpu adjustment values are going to be the same for this task.
		if cpuMaxMin[0] == cpuMaxMin[1] {
			cpuAdjustmentValues[taskId] = []int64{cpuMaxMin[0], cpuMaxMin[0], cpuMaxMin[0], cpuMaxMin[0], cpuMaxMin[0]}
		} else {
			cpuAdjustmentValues[taskId] = make([]int64, 5)
			cpuMin := cpuMaxMin[1]
			// Given that
			// 1. cpu_quota is expressed as a multiple of cpu_period (as default 100k), and
			// 2. scaling granularity is 1cpu,
			// dividing by 4 is guaranteed to yield a whole number.
			subRangeDelta := (cpuMaxMin[0] - cpuMin) / 4
			for i := 0; i < 5; i++ {
				cpuAdjustmentValues[taskId][i] = cpuMin + (int64(i) * subRangeDelta)
			}
		}
		elapsedTime := now.Sub(runningTasksInfo[taskId].submittedTime).Seconds()
		rankedTasks = append(rankedTasks, &avgIrrRecommendationItem{
			taskId: taskId,
			remainingTTDSeconds: int64(math.Max(
				0, float64(runningTasksInfo[taskId].totalAvailableTime)-elapsedTime)),
		})
	}

	heap.Init(&rankedTasks)
	return rankedTasks, cpuAdjustmentValues
}

func (u *AvgIRRBasedVScalerResourceUpdater) selectCpuAdjustment(
	recItem *avgIrrRecommendationItem,
	totalAvailableTime int64,
	cpuAdjustmentValues []int64) int64 {

	log.Printf("taskId=%s, totalAvailableTime=%d, remainingTTDSeconds=%d, cpuAdjustments=%v",
		recItem.taskId, totalAvailableTime, recItem.remainingTTDSeconds, cpuAdjustmentValues)

	remainingTime := recItem.remainingTTDSeconds
	elapsedPortion := math.Round((1-(float64(remainingTime)/float64(totalAvailableTime)))*100.0) / 100.0
	var selectedCpu int64

	if elapsedPortion >= 0.8 {
		selectedCpu = cpuAdjustmentValues[4]
	} else if elapsedPortion >= 0.6 {
		selectedCpu = cpuAdjustmentValues[3]
	} else if elapsedPortion >= 0.4 {
		selectedCpu = cpuAdjustmentValues[2]
	} else if elapsedPortion >= 0.2 {
		selectedCpu = cpuAdjustmentValues[1]
	} else {
		selectedCpu = cpuAdjustmentValues[0]
	}

	return selectedCpu
}

func (u *AvgIRRBasedVScalerResourceUpdater) Execute(
	runningTasksInfo map[TaskId]*runningTaskInfo,
	recommendations map[TaskId][]int64) {

	rankedTasks, cpuAdjustmentValues := u.rank(runningTasksInfo, recommendations)
	for len(rankedTasks) > 0 {
		item := heap.Pop(&rankedTasks)
		taskId := item.(*avgIrrRecommendationItem).taskId

		selectedCpu := u.selectCpuAdjustment(
			item.(*avgIrrRecommendationItem),
			runningTasksInfo[taskId].totalAvailableTime,
			cpuAdjustmentValues[taskId])

		if selectedCpu != runningTasksInfo[taskId].cpuQuotaAllocation {
			_, err := u.dockerCli.ContainerUpdate(u.ctx, string(runningTasksInfo[taskId].containerId), container.UpdateConfig{
				Resources: container.Resources{
					CPUQuota: selectedCpu,
				},
			})

			if err != nil {
				log.Printf("failed to scale up container[%s]", runningTasksInfo[taskId].containerId)
			} else {
				u.Log(
					string(taskId), string(runningTasksInfo[taskId].containerId),
					time.Now(),
					recommendations[taskId][0], recommendations[taskId][1],
					runningTasksInfo[taskId].cpuQuotaAllocation, selectedCpu)
				runningTasksInfo[taskId].cpuQuotaAllocation = selectedCpu
				log.Printf("scaled %s to cpu=%d", taskId, selectedCpu)
			}
		}
	}
}

func (*AvgIRRBasedVScalerResourceUpdater) Log(
	taskId string,
	containerId string,
	now time.Time,
	cpuMax, cpuMin int64,
	curCpuQuota, newCpuQuota int64) {

	var predAvgIrr float64
	var err error
	if _, ok := behaviorPredictors[TaskId(taskId)]; ok {
		predAvgIrr, err = behaviorPredictors[TaskId(taskId)].Predict([]float64{float64(newCpuQuota)})
		if err != nil {
			log.Printf("failed to predict application behavior: %v", err)
			predAvgIrr = 0
		}
	} else {
		log.Printf("behavior not predicted yet")
	}
	aLog.WithFields(logrus.Fields{
		"resource_updater":       avgIrrUpdater,
		"timestamp":              strconv.FormatInt(now.UnixNano(), 10),
		"taskId":                 taskId,
		"containerId":            containerId,
		"max_cpu_recommendation": strconv.FormatInt(cpuMax, 10),
		"min_cpu_recommendation": strconv.FormatInt(cpuMin, 10),
		"current_cpu_quota":      strconv.FormatInt(curCpuQuota, 10),
		"adjusted_cpu_quota":     strconv.FormatInt(newCpuQuota, 10),
		"predicted_avg_irr":      strconv.FormatFloat(predAvgIrr, 'e', -1, 64),
	}).Log(logrus.InfoLevel)
}

// PhasedOscillationVScalerResourceUpdater offers a phased vertical scaling policy.
// Applications are scaled following a trend. So, if the application is being scaled down, then
// the attempt to scale down again unless we hit the minimum.
// If the application is being scaled up then attempt to scale up until we hit the max.
// Applications are scaled gradually and not all the way all at once.
// The scaling step depends on how apart max and min recommendations are.
// The scaling step will always be in range [min, max-min].
// The scaling step will always in increments of 20% of max-min recommendations.
type PhasedOscillationVScalerResourceUpdater struct {
	hostMetricStore metrics.Store
	taskMetricStore metrics.Store
	dockerCli       *client.Client
	ctx             context.Context
	// adjustmentTrend stores the current scaling trend (up=true, down=false)
	adjustmentTrend map[TaskId]bool
}

// Initialize the resource updater.
func (u *PhasedOscillationVScalerResourceUpdater) Init(
	hostMetricStore, taskMetricStore metrics.Store,
	cli *client.Client, ctx context.Context) {

	u.hostMetricStore = hostMetricStore
	u.taskMetricStore = taskMetricStore
	u.dockerCli = cli
	u.ctx = ctx
	u.adjustmentTrend = make(map[TaskId]bool)
}

func (u *PhasedOscillationVScalerResourceUpdater) Execute(
	runningTasksInfo map[TaskId]*runningTaskInfo,
	recommendations map[TaskId][]int64) {

	for taskId, rec := range recommendations {
		cpuMax := rec[0]
		cpuMin := rec[1]
		log.Printf("max:min = %d:%d", cpuMax, cpuMin)
		containerId := string(runningTasksInfo[taskId].containerId)
		if cpuMax == cpuMin {
			// we have no choice but to scale to cpuMax (= cpuMin).
			_, err := u.dockerCli.ContainerUpdate(u.ctx, containerId, container.UpdateConfig{
				Resources: container.Resources{
					CPUQuota: cpuMax,
				},
			})
			if err != nil {
				log.Printf("failed to scale up container[%s]", containerId)
			} else {
				u.Log(
					string(taskId), containerId,
					time.Now(),
					cpuMax, cpuMin,
					runningTasksInfo[taskId].cpuQuotaAllocation, cpuMax)
				runningTasksInfo[taskId].cpuQuotaAllocation = cpuMax
				log.Printf("scaled %s to cpu=%d", taskId, cpuMax)
			}
			// setting scale down trend for the next cycle.
			u.adjustmentTrend[taskId] = false
		} else {
			step := u.step(cpuMax, cpuMin)
			var selectedCpu int64 = -1 // invalid value.
			currentCpu := runningTasksInfo[taskId].cpuQuotaAllocation
			if positiveTrend, ok := u.adjustmentTrend[taskId]; !ok {
				// default scale down first.
				selectedCpu = u.max(currentCpu-step, cpuMin)
				u.adjustmentTrend[taskId] = false
			} else {
				if !positiveTrend {
					if (currentCpu - step) <= cpuMin {
						// need to update the trend for the next cycle.
						u.adjustmentTrend[taskId] = true
					}
					selectedCpu = u.max(currentCpu-step, cpuMin)
				} else {
					if (currentCpu + step) >= cpuMax {
						// need to update the trend for the next cycle.
						u.adjustmentTrend[taskId] = false
					}
					selectedCpu = u.min(currentCpu+step, cpuMax)
				}
			}
			log.Printf("step:%d, selected_cpu:%d, trend:%t", step, selectedCpu, u.adjustmentTrend[taskId])
			if selectedCpu == -1 {
				// should not be here.
				log.Printf("failed to scale container[%s] with taskId[%s]", containerId, taskId)
			} else {
				_, err := u.dockerCli.ContainerUpdate(u.ctx, containerId, container.UpdateConfig{
					Resources: container.Resources{
						CPUQuota: selectedCpu,
					},
				})
				if err != nil {
					log.Printf("failed to scale container[%s] with taskId[%s]: %v", containerId, taskId, err)
				} else {
					u.Log(
						string(taskId), containerId,
						time.Now(),
						cpuMax, cpuMin,
						runningTasksInfo[taskId].cpuQuotaAllocation, selectedCpu)
					runningTasksInfo[taskId].cpuQuotaAllocation = selectedCpu
					log.Printf("scaled %s to cpu=%d", taskId, selectedCpu)
				}
			}
		}
	}
}

func (PhasedOscillationVScalerResourceUpdater) min(a, b int64) int64 {
	if a == b {
		return a
	}
	if a < b {
		return a
	}
	return b
}

func (PhasedOscillationVScalerResourceUpdater) max(a, b int64) int64 {
	if a == b {
		return a
	}
	if a < b {
		return b
	}
	return a
}

// stepRoundedToNearestCpu returns scaling step size as 10% of the step range, rounded to the nearest multiple of cpuperiod.
func (PhasedOscillationVScalerResourceUpdater) step(max, min int64) int64 {
	// no rationale behind picking 10% of range as step size.
	return int64(float64(max-min) * 0.1)
}

func (*PhasedOscillationVScalerResourceUpdater) Log(
	taskId string,
	containerId string,
	now time.Time,
	cpuMax, cpuMin int64,
	curCpuQuota, newCpuQuota int64) {
	aLog.WithFields(logrus.Fields{
		"resource_updater":       phasedOscUpdater,
		"timestamp":              strconv.FormatInt(now.UnixNano(), 10),
		"taskId":                 taskId,
		"containerId":            containerId,
		"max_cpu_recommendation": strconv.FormatInt(cpuMax, 10),
		"min_cpu_recommendation": strconv.FormatInt(cpuMin, 10),
		"current_cpu_quota":      strconv.FormatInt(curCpuQuota, 10),
		"adjusted_cpu_quota":     strconv.FormatInt(newCpuQuota, 10),
	}).Log(logrus.InfoLevel)
}

// Resource Updater factory.
const (
	naiveUpdater     = "naive"
	avgIrrUpdater    = "avg_irr"
	phasedOscUpdater = "phased_osc"
)

var resourceUpdaterFactory = map[string]VScalerResourceUpdater{
	naiveUpdater:     &NaiveVScalerResourceUpdater{},
	avgIrrUpdater:    &AvgIRRBasedVScalerResourceUpdater{},
	phasedOscUpdater: &PhasedOscillationVScalerResourceUpdater{},
}

func GetVScalerResourceUpdater(
	name string,
	hostMetricStore, taskMetricStore metrics.Store,
	cli *client.Client, ctx context.Context) (VScalerResourceUpdater, error) {

	var r VScalerResourceUpdater
	var ok bool
	if r, ok = resourceUpdaterFactory[name]; !ok {
		return nil, errors.New("invalid resource updater name provided")
	}

	if (hostMetricStore == nil) || (taskMetricStore == nil) {
		return nil, errors.New("valid metric stores need to be provided")
	}
	r.Init(hostMetricStore, taskMetricStore, cli, ctx)
	return r, nil
}
