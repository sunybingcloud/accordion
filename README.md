# Accordion

Accordion is a configurable framework to vertical scale docker containers.

### Components

##### Metric Store
Stores and maintains task/host metrics.

##### Metric Collector
Collect task/host metrics and store them in the corresponding metric store.

##### Vertical Scaler (VScaler)
Runs as a cron job and vertically scales one or more currently running tasks.
Vertical scaling involves two steps.

1. Analysis of task metrics and recommendation of resource adjustments. This is performed by a component called **VScalerRecommender**.
2. Selection of the scaling recommendations and updating the resource allocation of the respective container. This is performed by a component called **VScalerResourceUpdater**.

##### Logger
Log scaling recommendations and resource updates in csv format.

### Build
The project includes a [makefile](./makefile) that can be used to compile, run tests etc.
Run `make build executable=<name of executable>` to compile build an executable.

### Test
To run unit tests, you can run the following.

1. All unit tests: `make test`.
2. All unit tests with verbose log: `make test-verbose`.
3. Test single module: `make test-module module=<name of module>`.
4. Test single module with verbose log: `make test-module-verbose module=<name of module>`.

You can also use the makefile to format the code by running `make format`.

#### For Linux
Run `make build-linux` to compile and build an executable that can be run on linux machines.


### TODO
* Estimated values for factors in next window.
* smart recommender (provides range > 2 steps by factoring in metrics and deadlines).
* smart selector (determines #resources to take away and #resources to allocate additionally by ranking applications).
