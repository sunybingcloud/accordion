package def

import "time"

type Task struct {
	Name           string   `json:"name"`
	CpuQuota       float64  `json:"cpu_quota"`
	RAM            int      `json:"ram_MB"`
	Image          string   `json:"image"`
	CMD            []string `json:"cmd"`
	Instances      int      `json:"inst"`
	TimeToDeadline int64    `json:"deadline_sec"`
	SubmittedTime  time.Time
}
