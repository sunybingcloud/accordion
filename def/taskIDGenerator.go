package def

import (
	"github.com/satori/go.uuid"
	"strconv"
	"strings"
)

const IDDelimiter = "-"
const IDPrefix = "accordion"

// Generate a unique task ID given the following,
// 1. Task configuration (name, instances, etc.)
func GenerateTaskID(taskName string, instanceNum int) string {
	var elements []string
	// Appending the framework name.
	elements = append(elements, IDPrefix)
	// Appending the task name.
	elements = append(elements, taskName)
	// Appending the instance.
	elements = append(elements, strconv.Itoa(instanceNum))
	// Generating the UUID
	id := uuid.NewV4()
	elements = append(elements, id.String())
	// Joining elements with '-' as delimiter.
	return strings.Join(elements, IDDelimiter)
}
