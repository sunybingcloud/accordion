package main

import (
	"bitbucket.org/sunybingcloud/accordion/metrics"
	"context"
	"fmt"
	"github.com/docker/docker/client"
	"github.com/robfig/cron/v3"
	"github.com/stretchr/testify/assert"
	"math"
	"math/rand"
	"runtime"
	"testing"
	"time"
)

func TestGetVScalerRecommender(t *testing.T) {
	// Invalid recommender name.
	s := "?/5 * * * * *"
	sched, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(s)
	assert.NoError(t, err)
	assert.NotNil(t, sched)

	r, err := GetVScalerRecommender(
		"other",
		sched,
		metrics.NewHostMetricStore(10), metrics.NewTaskMetricStore(10), 0.5)
	assert.Error(t, err)
	assert.Nil(t, r)

	// Invalid metric stores.
	r, err = GetVScalerRecommender("naive", sched, nil, nil, 0.5)
	assert.Error(t, err)
	assert.Nil(t, r)

	// Valid.
	r, err = GetVScalerRecommender(
		"naive",
		sched,
		metrics.NewHostMetricStore(10),
		metrics.NewTaskMetricStore(10),
		0.5)
	assert.NoError(t, err)
	assert.NotNil(t, r)
}

func TestGetVScalerResourceUpdater(t *testing.T) {
	// Invalid resource updater name.
	s := "?/5 * * * * *"
	sched, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(s)
	assert.NoError(t, err)
	assert.NotNil(t, sched)

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	assert.NoError(t, err)
	assert.NotNil(t, cli)

	r, err := GetVScalerResourceUpdater(
		"other",
		metrics.NewHostMetricStore(10), metrics.NewTaskMetricStore(10), cli, ctx)
	assert.Error(t, err)
	assert.Nil(t, r)

	// Invalid metric stores.
	r, err = GetVScalerResourceUpdater("naive", nil, nil, cli, ctx)
	assert.Error(t, err)
	assert.Nil(t, r)

	// Valid.
	r, err = GetVScalerResourceUpdater(
		"naive",
		metrics.NewHostMetricStore(10),
		metrics.NewTaskMetricStore(10),
		cli, ctx)
	assert.NoError(t, err)
	assert.NotNil(t, r)
}

func getVScalerRecommenderForTesting(t *testing.T) VScalerRecommender {
	s := "?/5 * * * * *"
	sched, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(s)
	assert.NoError(t, err)
	assert.NotNil(t, sched)
	r, err := GetVScalerRecommender(
		"naive",
		sched,
		metrics.NewHostMetricStore(10),
		metrics.NewTaskMetricStore(10), 0.5)
	return r
}

func getVScalerResourceUpdaterForTesting(t *testing.T) VScalerResourceUpdater {
	s := "?/5 * * * * *"
	sched, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse(s)
	assert.NoError(t, err)
	assert.NotNil(t, sched)

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	assert.NoError(t, err)
	assert.NotNil(t, cli)

	r, err := GetVScalerResourceUpdater(
		"naive",
		metrics.NewHostMetricStore(10),
		metrics.NewTaskMetricStore(10),
		cli, ctx)
	return r
}

func TestNewVScaler(t *testing.T) {
	// Invalid with no options.
	s, err := NewVScaler()
	assert.Error(t, err)
	assert.Nil(t, s)

	// Invalid schedule.
	t.Run("scaling schedule not provided", func(t *testing.T) {
		s, err = NewVScaler(
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	// Invalid scaling limits.
	t.Run("invalid scaling limits", func(t *testing.T) {
		t.Run("max > #cpus", func(t *testing.T) {
			s, err = NewVScaler(
				WithSchedule("?/1 * * * * *"),
				WithScalingLimits(int64(runtime.NumCPU()+1), 1),
				WithRecommender(getVScalerRecommenderForTesting(t)),
				WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
				WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
			assert.Error(t, err)
			assert.Nil(t, s)
		})

		t.Run("min <= 0", func(t *testing.T) {
			s, err = NewVScaler(
				WithSchedule("?/1 * * * * *"),
				WithScalingLimits(int64(runtime.NumCPU()), 0),
				WithRecommender(getVScalerRecommenderForTesting(t)),
				WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
				WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
			assert.Error(t, err)
			assert.Nil(t, s)
		})
	})

	// Invalid recommender.
	t.Run("invalid recommender", func(t *testing.T) {
		// nil.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(nil),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)

		// not provided.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	t.Run("invalid resource updater", func(t *testing.T) {
		// nil.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(nil),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)

		// not provided.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	t.Run("invalid profiling window", func(t *testing.T) {
		// < 1.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(0), WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)

		// not provided.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithPeriodicProfiling(true, 2))
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	t.Run("invalid periodic profiling interval cycle", func(t *testing.T) {
		// < 1.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 0))
		assert.Error(t, err)
		assert.Nil(t, s)

		// < 0.
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, -1))
		assert.Error(t, err)
		assert.Nil(t, s)
	})

	// Valid.
	t.Run("all options valid", func(t *testing.T) {
		s, err = NewVScaler(
			WithSchedule("?/1 * * * * *"),
			WithScalingLimits(int64(runtime.NumCPU()), 1),
			WithRecommender(getVScalerRecommenderForTesting(t)),
			WithResourceUpdater(getVScalerResourceUpdaterForTesting(t)),
			WithProfilingWindow(5), WithPeriodicProfiling(true, 2))
		assert.NoError(t, err)
		assert.NotNil(t, s)
		// validating fields.
		now := time.Now()
		one := s.scalingSchedule.Next(now)
		two := s.scalingSchedule.Next(one)
		assert.Equal(t, float64(1), two.Sub(one).Seconds())
		assert.Equal(t, int64(runtime.NumCPU()), s.scaleLimitCpuMax)
		assert.Equal(t, int64(1), s.scaleLimitCpuMin)
		assert.True(t, s.periodicProfiling)
		assert.Equal(t, int64(2), s.profilingIntervalCycles)
	})
}

func TestProfiling(t *testing.T) {
	p := &Profiling{
		inProgress:      true,
		cyclesRemaining: 5,
	}
	assert.NotNil(t, p)
	assert.True(t, p.IsInProgress())
	assert.False(t, p.HasFinished())
	assert.Equal(t, int64(5), p.cyclesRemaining)
	cr := p.cyclesRemaining
	for i := 0; i < 5; i++ {
		assert.True(t, p.IsInProgress())
		assert.Equal(t, cr, p.cyclesRemaining)
		p.Do()
		cr--
	}
	assert.Zero(t, p.cyclesRemaining)
	p.Stop()
	assert.True(t, p.HasFinished() && !p.IsInProgress())
}

func TestBacklog(t *testing.T) {
	b := newBacklog()
	b.avgIrrEstNoScale = 10

	assert.NotNil(t, b)
	assert.Zero(t, b.ExecTime())
	assert.Zero(t, b.value)
	// accruing backlog.
	var metricSchedSeconds int64 = 1
	var scalingSchedSeconds int64 = 5
	var expectedInstr = 10 * scalingSchedSeconds * metricSchedSeconds
	var expectedBkl int64 = 0
	var expectedExecTime float64 = 0
	var bklTimeSpent float64 = 0
	rand.Seed(time.Now().Unix())
	for i := 0; i < 5; i++ {
		values := rand.Perm(5)
		// making sure that values contains numbers in range [1, n)
		// rand.Perm returns numbers in range [0, n).
		for i := range values {
			if values[i] == 0 {
				values[i] = 1
			}
		}
		var irrDataPoints []float64
		var netInstr int64 = 0
		for _, v := range values {
			irrDataPoints = append(irrDataPoints, float64(v))
			netInstr += int64(v) * metricSchedSeconds
		}
		b.ConsumeAndAccrue(irrDataPoints, metricSchedSeconds, scalingSchedSeconds, true)
		if expectedBkl == 0 {
			expectedBkl = expectedInstr - netInstr
			expectedExecTime = float64(expectedBkl) / b.avgIrrEstNoScale
		} else {
			var consumedBkl int64 = 0
			var toConsume = b.value
			var additionalTimeTaken float64 = 0
			for _, v := range values {
				nRetired := int64(v) * metricSchedSeconds
				if toConsume > 0 {
					portionConsumed := int64(math.Min(float64(nRetired), float64(toConsume)))
					t := math.Round((float64(portionConsumed)/float64(v))*10) / 10
					if !math.IsNaN(t) {
						additionalTimeTaken += t
						consumedBkl += portionConsumed
						toConsume -= portionConsumed
					}
				}
			}
			expectedBkl = (expectedBkl - consumedBkl) + int64(math.Max(0, float64(expectedInstr-(netInstr-consumedBkl))))
			bklTimeSpent += additionalTimeTaken
			expectedExecTime = bklTimeSpent + (float64(expectedBkl) / b.avgIrrEstNoScale)
		}

		// checking backlog execution time.
		assert.Equal(t, math.Round((float64(expectedBkl)/b.avgIrrEstNoScale)*10)/10, b.ExecTime())
		assert.Equal(t, math.Round(expectedExecTime*10)/10, b.TotalExecTime())
	}
}

func TestAvgIRRBasedVScalerRecommender_Execute(t *testing.T) {
	t.Log("initializing the metric stores")
	const historyCap = 5
	hostMetricStore := metrics.NewHostMetricStore(historyCap)
	taskMetricStore := metrics.NewTaskMetricStore(historyCap)
	// Populating task metrics for 5 tasks into metric store
	// task-1 {cpu_quota=1cpu, irr=[10, 20, 30, 40, 50], ttd=10}
	// task-2 {cpu_quota=2cpu, irr=[20, 30, 40, 50, 60], ttd=20}
	// task-3 {cpu_quota=3cpu, irr=[30, 40, 50, 60, 70], ttd=30}
	// task-4 {cpu_quota=4cpu, irr=[40, 50, 60, 70, 80], ttd=40}
	// task-5 {cpu_quota=5cpu, irr=[50, 60, 70, 80, 90], ttd=50}
	t.Log("populating task metrics for 5 tasks into the metric store")
	rti := make(map[TaskId]*runningTaskInfo)
	for i := 1; i <= 5; i++ {
		taskId := fmt.Sprintf("task-%d", i)
		start := i * 10
		end := start + 40
		for j := start; j <= end; j += 10 {
			err := taskMetricStore.(*metrics.TaskMetricStore).Take(taskId,
				metrics.InstructionRetirementRate, float64(j))
			assert.NoError(t, err)
		}
		err := taskMetricStore.(*metrics.TaskMetricStore).Take(taskId, metrics.CpuQuota, float64(int64(i)*CpuPeriod))
		assert.NoError(t, err)
		now := time.Now()
		rti[TaskId(taskId)] = &runningTaskInfo{
			cpuQuotaAllocation: int64(i) * CpuPeriod,
			submittedTime:      now,
			totalAvailableTime: int64(i) * 10,
		}
	}

	// printing running task information.
	for taskId, info := range rti {
		irrValues, err := taskMetricStore.(*metrics.TaskMetricStore).LatestN(
			string(taskId), metrics.InstructionRetirementRate, historyCap)
		assert.NoError(t, err)
		fmt.Printf("\t\t%s {cpu_quota=%d, irr=%v, ttd=%d}\n",
			taskId, info.cpuQuotaAllocation, irrValues, info.totalAvailableTime)
	}

	// initializing avg_irr recommender.
	t.Log("initializing avg_irr recommender")
	r := &AvgIRRBasedVScalerRecommender{}
	// metrics fetched every 1 second.
	metricSchedule, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse("?/1 * * * * *")
	assert.NoError(t, err)
	r.Init(metricSchedule, hostMetricStore, taskMetricStore, 0.5)
	assert.Equal(t, metricSchedule, r.metricSchedule)
	assert.Equal(t, int64(1), r.metricScheduleSeconds)
	assert.Equal(t, taskMetricStore, r.taskMetricStore)
	assert.Equal(t, hostMetricStore, r.hostMetricStore)

	// Initializing avg_irr resource updater.
	t.Log("initializing avg_irr resource updater")
	u, err := GetVScalerResourceUpdater("avg_irr", hostMetricStore, taskMetricStore, nil, nil)
	assert.NoError(t, err)
	assert.NotNil(t, u)

	// Initializer VScaler.
	t.Log("initializing scaler")
	const profilingWindow = 1 // profiling for 1 scaling cycle.
	const periodicProfiling = true
	const profilingIntervalCycles = 2
	const scalingScheduleString = "?/5 * * * * *"
	const scalingScheduleSeconds = 5
	scalingSchedule, err := cron.NewParser(
		cron.SecondOptional | cron.Minute | cron.Hour | cron.Dom | cron.Month | cron.Dow).Parse("?/5 * * * * *")
	assert.NoError(t, err)
	scaler, err := NewVScaler(
		WithSchedule(scalingScheduleString),
		WithProfilingWindow(profilingWindow),
		WithPeriodicProfiling(periodicProfiling, profilingIntervalCycles),
		WithScalingLimits(8, 1),
		WithRecommender(r),
		WithResourceUpdater(u))
	assert.NoError(t, err)
	scaler.runningTasks = rti
	scaler.scalingSchedule = scalingSchedule

	// Round 1 - Immediately after profiling.
	// Simulating elapse of one profiling cycle.
	<-time.After(profilingWindow * scalingScheduleSeconds * time.Second)
	var recommendations map[TaskId][]int64
	t.Log("ROUND-1 - IMMEDIATELY AFTER PROFILING")
	recommendations = r.Execute(scaler)
	assert.NotNil(t, recommendations)
	// verifying state of data structures in recommender.
	assert.Equal(t, int64(5), r.scalingScheduleSeconds)
	for i := 1; i <= 5; i++ {
		taskId := TaskId(fmt.Sprintf("task-%d", i))
		m, ok := r.metadata[taskId]
		assert.True(t, ok)
		assert.NotNil(t, m.bkl)
		avgIrrEstNoScale := float64((((i * 10) + 40) + (i * 10)) / 2)
		assert.Equal(t, avgIrrEstNoScale, m.bkl.avgIrrEstNoScale)
		assert.True(t, m.profiling.HasFinished())
		assert.Equal(t, int64(0), m.profiling.cyclesRemaining)
		assert.NotNil(t, m.irrRegression)
		assert.Equal(t, nCPU(int64(i)), m.requestedCpuQuotaAllocation)
		rec, ok := recommendations[taskId]
		assert.True(t, ok)
		expectedCpuMin := int64(math.Max(
			float64(nCPU(scaler.scaleLimitCpuMin)),
			float64(rti[taskId].cpuQuotaAllocation-nCPU(2))))
		assert.Equal(t, m.requestedCpuQuotaAllocation, rec[0])
		assert.Equal(t, expectedCpuMin, rec[1])
		elapsedTimeSeconds := scaler.profilingWindow * r.scalingScheduleSeconds
		assert.Equal(t,
			rti[taskId].totalAvailableTime-elapsedTimeSeconds,
			m.remainingTTDSeconds)
	}

	t.Log("ranking the scaling recommendations")
	rankedTasks, cpuAdjustmentValues := u.(*AvgIRRBasedVScalerResourceUpdater).rank(rti, recommendations)
	assert.NotNil(t, cpuAdjustmentValues)
	assert.Equal(t, 5, len(rankedTasks))
	expectedCpuAdjustmentValues := map[TaskId][]int64{
		"task-1": {100000, 100000, 100000, 100000, 100000},
		"task-2": {100000, 125000, 150000, 175000, 200000},
		"task-3": {100000, 150000, 200000, 250000, 300000},
		"task-4": {200000, 250000, 300000, 350000, 400000},
		"task-5": {300000, 350000, 400000, 450000, 500000},
	}
	for taskId := range expectedCpuAdjustmentValues {
		_, ok := cpuAdjustmentValues[taskId]
		assert.True(t, ok)
	}

	for taskId, actual := range cpuAdjustmentValues {
		expected := expectedCpuAdjustmentValues[taskId]
		assert.Equal(t, len(expected), len(actual))
		for i := 0; i < len(expected); i++ {
			assert.Equal(t, expected[i], actual[i])
		}
	}

	for _, recItem := range rankedTasks {
		actualSelectedCpu := u.(*AvgIRRBasedVScalerResourceUpdater).selectCpuAdjustment(
			recItem,
			rti[recItem.taskId].totalAvailableTime,
			cpuAdjustmentValues[recItem.taskId])
		rangeCondition := (actualSelectedCpu >= cpuAdjustmentValues[recItem.taskId][0]) &&
			(actualSelectedCpu <= cpuAdjustmentValues[recItem.taskId][4])
		assert.True(t, rangeCondition)

		portionElapsed := math.Round(
			(1-(float64(recItem.remainingTTDSeconds)/
				float64(rti[recItem.taskId].totalAvailableTime)))*100.0) / 100.0
		if portionElapsed >= 0.8 {
			assert.Equal(t, expectedCpuAdjustmentValues[recItem.taskId][4], actualSelectedCpu)
		} else if portionElapsed >= 0.6 {
			assert.Equal(t, expectedCpuAdjustmentValues[recItem.taskId][3], actualSelectedCpu)
		} else if portionElapsed >= 0.4 {
			assert.Equal(t, expectedCpuAdjustmentValues[recItem.taskId][2], actualSelectedCpu)
		} else if portionElapsed >= 0.2 {
			assert.Equal(t, expectedCpuAdjustmentValues[recItem.taskId][1], actualSelectedCpu)
		} else {
			assert.Equal(t, expectedCpuAdjustmentValues[recItem.taskId][0], actualSelectedCpu)
		}
		fmt.Printf("\t\t%s {selected_cpu=%d, portion_elapsed=%f, totalAvailTime=%d, remainingTTDSeconds=%d}\n",
			recItem.taskId,
			actualSelectedCpu,
			portionElapsed,
			rti[recItem.taskId].totalAvailableTime,
			recItem.remainingTTDSeconds)
	}
}
